<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;

class Carrinho extends Model {

    public $timestamps = false;
    public $usuario;

    // Retorna os produtos do carrinho atual
    public function scopeCarrinhoAtual($query) {
        if ($this->usuario > 0) {
            $id = $this->usuario;
        } else {
            $id = \Auth::user()->id;
        }
        $carrinhos = Carrinho::where('pedido', 0)->where(
                'user_id', $id);

        $carrinhos = $carrinhos->select(DB::raw('COUNT(*) as quantidade', 'product_id'), 'product_id', 'img', 'carrinhos.id', 'nome', 'preco', 'peso')->join('produtos', 'produtos.id', '=', 'carrinhos.product_id')->groupBy('product_id')->get()->toArray();

        $produto = [];
        foreach ($carrinhos as $key => $dado) {

            $array = $dado;
            $array['produto'] = $dado;

            $produto[] = $array;
        }


        return $produto;
    }

    // Retorna os produtos do carrinho
    public function scopeCarrinhoPedido($query, $id) {
        $carrinhos = Carrinho::where('pedido', $id)->get();
        return $this->pegarProdutos($carrinhos);
    }

    // Define um pedido para o carrinho
    public function scopePedido($query, $id) {
        $carrinhos = $query->where('pedido', 0);
        if (!$carrinhos->first()) {
            return;
        }

        foreach ($carrinhos->get() as $carrinho) {
            $c = Carrinho::find($carrinho['id']);
            $c->pedido = $id;
            $c->save();
        }
    }

    // Retorna os produtos de algum carrinho
    public function pegarProdutos($carrinhos) {
        
    }

}
