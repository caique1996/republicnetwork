<?php

class UnilevelHelper {

    public static $usuariosSuperiores = [];
    public static $usuarios_rede = [];

    /**
     *  Pegar usuários superiores.Antes de utiliza,utile o metodo limpaRede()
     *
     * @param int $user_id  Id do usuário.
     *
     * @return array .
     */
    static function getUsuariosSuperiores($user_id) {
        $user = \App\User::where('id', $user_id);
        if ($user->count() > 0) {
            self::$usuariosSuperiores[] = $user->first()->pai_id;
            self::getUsuariosSuperiores($user->first()->pai_id);
        }
        return self::$usuariosSuperiores;
    }

    /**
     *  Limpa a rede para uma nova consulta.
     *
     *
     * @return void .
     */
    static function limpaRede() {
        self::$usuariosSuperiores = [];
        self::$usuarios_rede = [];
    }

    /**
     * Retorna usuários da rede.Antes de utiliza,utile o metódo limpaRede()
     *
     * @param int $user_id Id do usuário.
     *
     * @return array usuários ordenados por niveis.
     */
    public static function getUsuariosRede($user_id, $nivel = 1, $limit = 9999999999) {
        if ($limit >= $nivel) {
            $indicados = \App\User::where('pai_id', $user_id);

            if ($indicados->count() > 0) {
                foreach ($indicados->get() as $user) {
                    self::$usuarios_rede[$nivel][] = ['userData' => $user, 'nivel' => $nivel];
                }
            }
            $nivel = $nivel + 1;
            foreach ($indicados->get() as $user) {
                self::getUsuariosRede($user->id, $nivel, $limit);
            }
            return self::$usuarios_rede;
        }
    }

}
