<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Trade;
use App\Http\Controllers\Controller;
use Validator;

class TradesController extends Controller {

    public function index() {
        $trades = Trade::all();

        return view('admin.pages.trade', compact('trades'));
    }

    public function create() {
        return view('admin.pages.trade.create');
    }

    public function view($id) {
        $dados['dados'] = Trade::where('id', $id)->first();
        return view('admin.pages.trade.edit', $dados);
    }

    public function salvar() {
        $data = \Input::all();

        $valida = [
            'id' => 'integer|required',
            'username' => 'required|exists:users|max:255',
            'status' => 'required',
            'nome_exibicao' => 'required'
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);
            Trade::where('id', $id)->update($data);
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Trade Atualizado
                 </div>
EOL;
        }
    }

    public function store(Request $request) {
        $trade = new Trade();

        $trade->status = $request->status;
        $trade->username = $request->username;
        $trade->nome_exibicao = $request->nome_exibicao;
        $data = $request->all();

        $valida = [
            'username' => 'required|exists:users|unique:trades|max:255',
            'status' => 'required',
            'nome_exibicao' => 'required|unique:trades'
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            if ($trade->save()) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Trade Adicionado
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
            }
        }
    }

}
