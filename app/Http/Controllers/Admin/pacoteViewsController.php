<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Trade;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\pacoteViews;
use App\Pacote;

class pacoteViewsController extends Controller {

    public function index() {
        $usr = new User();

        $pacotes = pacoteViews::all();
        return view('admin.pages.pacote_views', compact('pacotes'));
    }

    public function create() {
        return view('admin.pages.pacotesViews.create');
    }

    public function view($id) {
        $dados['dados'] = pacoteViews::where('id', $id)->first();
        return view('admin.pages.pacotesViews.edit', $dados);
    }

    public function store(Request $request) {
        $pacote = new pacoteViews();

        $pacote->status = $request->status;
        $pacote->nome = $request->nome;
        $pacote->views = $request->views;
        $pacote->valor = $request->valor;

        $data = $request->all();

        $valida = [
            'status' => 'required',
            'valor' => 'required',
            'views' => 'integer|required'
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            if ($pacote->save()) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Adicionado
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
            }
        }
    }

    public function salvar() {
        $data = \Input::all();

        $valida = [
            'id' => 'integer|required',
            'status' => 'required',
            'valor' => 'required',
            'views' => 'integer|required'
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);
            pacoteViews::where('id', $id)->update($data);
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Atualizado
                 </div>
EOL;
        }
    }

}

?>