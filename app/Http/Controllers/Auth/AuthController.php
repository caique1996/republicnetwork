<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'username' => 'required|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'username' => $data['username'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function showLoginUser() {
        return view('painel.auth.login');
    }

    public function logout() {
        if (Auth::user()->admin == 1) {
            \Auth::guard($this->getGuard())->logout();
            \Session::flash('success', 'Tchau,já estamos com saudades :-( ');

            return redirect('/admin/login');
        } else {
                        \Auth::guard($this->getGuard())->logout();

            \Session::flash('success', 'Tchau,já estamos com saudades :-( ');

            return redirect('/painel/login');
        }
    }

    public function login_user(Request $request) {
        if (\App\config::getConf()['recaptcha_status'] == 'sim') {
            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . \App\config::getConf()['recaptcha_secret'] . "&response=" . $request->all()['g-recaptcha-response']);
            $res = json_decode($json);
            if ($res->success <> 1) {
                return redirect('/painel/login')->withErrors(['Captcha inválido!']);
            }
        }
        $validator = Validator::make($request->all(), ['username' => 'required', 'password' => 'required']);
        if ($validator->fails()) {
            return redirect('/painel/login')->withErrors($validator);
        } else {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'admin' => 0])) {
                return redirect()->intended('painel/home');
            } else {
                return redirect('/painel/login')->withErrors(['Credenciais informadas não correspondem com nossos registros.']);
            }
        }
    }

    public function login_admin(Request $request) {
        if (\App\config::getConf()['recaptcha_status'] == 'sim') {
            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . \App\config::getConf()['recaptcha_secret'] . "&response=" . $request->all()['g-recaptcha-response']);
            $res = json_decode($json);
            if ($res->success <> 1) {
                return redirect('/painel/login')->withErrors(['Captcha inválido!']);
            }
        }
        $validator = Validator::make($request->all(), ['username' => 'required', 'password' => 'required']);
        if ($validator->fails()) {
            return redirect('/admin/home')->withErrors($validator);
        } else {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'admin' => 1])) {
                return redirect()->intended('painel/home');
            } else {
                return redirect('/admin/login')->withErrors(['Credenciais informadas não correspondem com nossos registros.']);
            }
        }
    }

}
