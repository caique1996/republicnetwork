<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Binario;
use App\User;
use App\extratos;
use App\Pacote;
use App\Referrals;
use App\config;

class BonusController extends Controller {

    /**
     * Paga o bônus de equipe.
     *
     * @return void
     */
    public function bonusEquipe() {
        $usuariosAtivos = \App\User::where('ativo', 1)->where('graduacao', '>', 0)->get();
        $hoje = date('Y-m-d');
        $pontosModel = new \App\Ponto();
        foreach ($usuariosAtivos as $usuario) {
            $graduacaoUser = $usuario->getGraduacaoAtual();

            if (isset($graduacaoUser['id'])) {
                if ($hoje == $usuario->validade_pacote) {
                    $retorno = $usuario->infoConsumo();
                    if (!$retorno['delete']) {
                        $pontosEquipe = $usuario->pontosEquipe();
                        $valorBonus = $pontosEquipe * $graduacaoUser['porcentagem_equipe'];
                        $usuario->addSaldo($usuario->id, $valorBonus, 'Bônus de equipe', 18);
                    } /* else {
                      if (isset($usuario->getPacote()['id'])) {
                      $nova_data = date('Y-m-d', strtotime('+ ' . $usuario->getPacote()->duracao_meses . ' month', strtotime(date('Y-m-d'))));
                      $usuario->update(['validade_pacote' => $nova_data]);
                      $pontoModel->zeraPontuacao($user_id);
                      }
                      } */
                }
            }
        }
    }

    /**
     * Paga o bônus binário.
     *
     * @return void
     */
    public function bonusBinario() {
        $pacote = new Pacote();
        $Bin = new Binario();
        $usr = new User();
        $config = new config();
        $config = $config->getConfig();
        $binario_val = $config['binario_valor'];
        $usuarios = User::where('ativo', 1)->get();
        $bonusController = new BonusController();
        foreach ($usuarios as $value) {
            if ($usr->aptoBonusBinario($value['id'])) {
                $BinarioEsq = $Bin->totalEsquerda($value['id']);
                $BinarioDireita = $Bin->totalDireita($value['id']);
                $pacote = Pacote::where('id', $value['pacote'])->first();
                if (($BinarioDireita > $BinarioEsq or $BinarioDireita == $BinarioEsq)) {
                    $valorBinario = $BinarioEsq * $binario_val;
                    $usr->removePontos($value['id'], $BinarioEsq, 'esquerda');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(esquerda)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * 0.1), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioEsq, 'direita');
                } else if ($BinarioEsq > $BinarioDireita) {
                    $valorBinario = $BinarioDireita * $binario_val;
                    $usr->removePontos($value['id'], $BinarioDireita, 'direita');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(direita)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * 0.1), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioDireita, 'esquerda');
                }
            }
        }
    }

    /**
     * Executa divisão de lucros.
     *
     * @return void
     */
    public function divideLucro(Request $request) {
        $pacote = new Pacote();
        $today = date('Y-m-d');
        if ($request->pacote_div == 'todos') {
            $users = User::where('ativo', 1)->get();
        } else {
            $users = User::where('ativo', 1)->where('pacote', $request->pacote_div)->get();
        }

        $valor_simulacao = 0;
        if ($request->simulacao == 1) {
            foreach ($users as $value) {

                if ($value->apto_bonus()) {
                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {

                        $valor = $request->valor * $value->totalCotas($value['id']);
                    } else {
                        $valor = $request->valor * 1;
                    }
                    $desc = "Divisão de lucros";
                    //\Auth::user()->addSaldo($value['id'], $valor, $desc, 12);
                    $valor_simulacao+=$valor;
                }
            }
            echo "Você irá  dividir R$" . number_format($valor_simulacao, 2) . ".Mude a ação para dividir e realize esse procedimento novamente para realizar a divisão";
        } else {
            foreach ($users as $value) {
                if ($value->apto_bonus()) {
                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                        $valor = $request->valor * $value->totalCotas($value['id']);
                    } else {
                        $valor = $request->valor * 1;
                    }
                    $desc = "Divisão de lucros";
                    $value->addSaldo($value['id'], $valor, $desc, 12);
                    $valor_simulacao+=$valor;
                }
            }
            extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor_simulacao) * (-1)), 'beneficiado' => 1, 'tipo' => 12]);
            echo "Operação realizada com sucesso. R$" . number_format($valor_simulacao, 2) . " foram distribuidos!";
        }
    }

}
