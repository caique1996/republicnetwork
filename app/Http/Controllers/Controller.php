<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mail;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

// Função para calcular o frete
    // Função para calcular o frete
    function calculaFrete($cod_servico, $cep_destino, $peso, $altura = '2', $largura = '11', $comprimento = '16', $valor_declarado = '0.50') {
        $altura = 2;
        $largura = '11';
        $comprimento = '16';
        # Código dos Serviços dos Correios
        # 41106 PAC sem contrato
        # 40010 SEDEX sem contrato
        # 40045 SEDEX a Cobrar, sem contrato
        # 40215 SEDEX 10, sem contrato

        $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx" .
                "?nCdEmpresa=&sDsSenha=&sCepOrigem=" . \App\User::where('id', 1)->first()->cep .
                "&sCepDestino=" . $cep_destino . "&nVlPeso=" . $peso .
                "&nCdFormato=1&nVlComprimento=" . $comprimento .
                "&nVlAltura=" . $altura . "&nVlLargura=" . $largura .
                "&sCdMaoPropria=n&nVlValorDeclarado=" . 17 .
                "&sCdAvisoRecebimento=n&nCdServico=" . $cod_servico .
                "&nVlDiametro=0&StrRetorno=xml";


        $xml = simplexml_load_file($correios);
        if ($xml->cServico->Valor > 0) {

            return 0;
        } else {
            return false;
        }
    }

    function getAmount($money) {
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '', $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }

    public function enviarEmail($user_id, $subject, $content) {
        /*$config = \App\config::getConf();
        $userInfo = \App\User::where('id', $user_id)->first();

        session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name'], 'subject' => $subject]);
        $dataMail['subject'] = $subject;
        $dataMail['content'] = $content;
        Mail::send('painel.auth.emails.email', $dataMail, function ($message) {
          $message->from(\App\config::getConf()['from_address'], \App\config::getConf()['from_name']);
          $message->to(session('sendEmail'), session('sendName'))->subject(session('subject'));
          });*/

        /*$userInfo['email'] = 'caiquemarcelinosouza@gmail.com';
        session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name'], 'subject' => $subject]);
        $dataMail['subject'] = $subject;
        $dataMail['content'] = $content;
        Mail::send('painel.auth.emails.email', $dataMail, function ($message) {
          $message->from(\App\config::getConf()['from_address'], \App\config::getConf()['from_name']);
          $message->to(session('sendEmail'), session('sendName'))->subject(session('subject'));
          });
               /* $userInfo['email'] = 'arthurbfj@gmail.com';
        session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name'], 'subject' => $subject]);
        $dataMail['subject'] = $subject;
        $dataMail['content'] = $content;
        Mail::send('painel.auth.emails.email', $dataMail, function ($message) {
          $message->from(\App\config::getConf()['from_address'], \App\config::getConf()['from_name']);
          $message->to(session('sendEmail'), session('sendName'))->subject(session('subject'));
          });*/
        
        
    }

}
