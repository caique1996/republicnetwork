<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class CronController extends Controller {

    /**
     * Paga os bônus do dia.
     *
     * @return void
     */
    public function pagarBonus() {
        $bonusController = new BonusController();

        $bonusController->bonusEquipe();
        $bonusController->bonusBinario();
        $pontoModel = new \App\Ponto();
        foreach (\App\User::where('ativo', 1)->get() as $user) {
            if (date('Y-m-d') == $user->validade_pacote) {
                $data = $user->infoConsumo();
                if (!$data['delete']) {
                    $nova_data = date('Y-m-d', strtotime('+ ' . $pacoteInfo['duracao_meses'] . ' month', strtotime(date('Y-m-d'))));
                    $userInfo->update(['graduacao' => 0, 'validade_pacote' => $nova_data]);
                    $pontoModel->addPplus($user_id);
                } else {
                    $nova_data = date('Y-m-d', strtotime('+ ' . $pacoteInfo['duracao_meses'] . ' month', strtotime(date('Y-m-d'))));
                    $userInfo->update(['graduacao' => 0, 'validade_pacote' => $nova_data]);
                    $this->enviarEmail($user->id, 'Aviso', 'Infelizmente a menta mensal não foi atingida e suas pontuações foram zeradas.Não fique triste,estamos começando um novo mês e você tem mais 30 dias para ganhar mais.');
                }
            }
        }
        foreach (\App\User::where('ativo', 1)->get() as $user) {
            if (date('Y-m-d') == $user->validade_pacote) {
                $pontoModel->zeraPontuacao($user_id);
            }
        }
    }

}
