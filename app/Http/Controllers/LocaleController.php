<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use URL;
use Cookie;
use Session;
class LocaleController extends Controller {

    public function setLocale($locale = 'pt-br') {
         $changeLocale = new ChangeLocale($request->input('lang'));
        $this->dispatch($changeLocale);

        return redirect()->back();
    }
  
}

?>