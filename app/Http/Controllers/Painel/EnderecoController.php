<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Carrinho;
use App\Produtos;
use App\Enderecos;
use Validator;

class EnderecoController extends Controller {

    public function index() {
        $enderecos = Enderecos::where('user_id', \Auth::user()->id)->get();
        if (@$carrinho = Carrinho::where('user_id', \Auth::user()->id)->get()) {  // Produtos do carrinho do usuário
            $produtos = Carrinho::carrinhoAtual();
        } else {
            $produtos = [];
        }

        // Calcula o subtotal
        $subtotal = 0;
        $pesoTotal = 0;
        $subtotal2 = 0;
        foreach ($produtos as $produto) {

            $quantidade = $produto['quantidade'];
            $preco = $produto['produto']['preco'] * $quantidade;
            if (\Auth::user()->meu_desconto($produto['product_id'])) {
                $subtotal +=\Auth::user()->meu_desconto($produto['product_id']) * $quantidade;
            } else {
                $subtotal+=$preco;
            }
            $subtotal2 += $preco;

            $pesoTotal += $quantidade * $produto['produto']['peso'];
        }
 

        // Calcula o frete
        // Calcula o frete
      

        if (\Input::has('error')) {
            $error = \Input::get('error', null);
        }

        return view('painel.pages.enderecos', compact('enderecos', 'error','pesoTotal','subtotal'));
    }

    public function add() {

        $dados = array('cep', 'cidade', 'endereco', 'estado', 'pais');
        $validator = Validator::make(\Request::all(), [
                    'cep' => 'required|numeric',
                    'cidade' => 'required',
                    'endereco' => 'required',
                    'pais' => 'required',
                    'bairro' => 'required',
                    'numero' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('painel/meu-carrinho/endereco')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            foreach ($dados as $key) {

                if (!\Input::has($key)) {
                    return redirect("/painel/meu-carrinho/endereco");
                }

                // Verifica se o cep é válido
                if ($key == 'cep' && !preg_match('/[0-9]{5,5}([- ]?[0-9]{4})?$/', \Request::input($key))) {
                    $error = '?error=O CEP inserido é inválido, utilize apenas números';
                    return redirect("/painel/meu-carrinho/endereco/$error");
                }

                $dados[$key] = \Request::input($key, null);
            }
            $dados = \Request::all();
            $endereco = new Enderecos;
            $endereco->user_id = \Auth::user()->id;
            $endereco->cep = $dados['cep'];
            $endereco->cidade = $dados['cidade'];
            $endereco->endereco = $dados['endereco'];
            $endereco->estado = $dados['estado'];
            $endereco->pais = $dados['pais'];
            $endereco->siglaEstado = $dados['estado'];
            $endereco->bairro = $dados['bairro'];
            $endereco->complemento = @$dados['complemento'];
            $endereco->numero = $dados['numero'];
            $endereco->save();
            return redirect("/painel/meu-carrinho/endereco/");
        }
    }

}
