<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Carrinho;
use App\Produtos;
use App\Pedidos;
use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Enderecos;

class MeusPedidosController extends Controller {

    public function pagSeguroConfig() {
        if (env('PAGSEGURO_SANDBOX') == true) {
            return ['token' => env('PAGSEGURO_TOKEN_SANDBOX'), 'email' => env('PAGSEGURO_EMAIL'), 'url' => 'sandbox.pagseguro.uol.com.br'];
        } else {
            return ['token' => env('PAGSEGURO_TOKEN_PROD'), 'email' => env('PAGSEGURO_EMAIL'), 'url' => 'pagseguro.uol.com.br'];
        }
    }

    public function loadPagseguro() {

        /* Variáveis para enviar */

        $this->add_field('email', $this->pagSeguroConfig()['email']);
        $this->add_field('token', $this->pagSeguroConfig()['token']);
        $this->add_field('currency', 'BRL');
        $this->add_field('redirectURL', env('PAGSEGURO_REDIRECT')); // certo
        $this->add_field('reviewURL', env('PAGSEGURO_REDIRECT')); // errado
        $this->add_field('notificationURL', env('PAGSEGURO_NOTIFICATION')); //notificação
    }

    public function add_field($field, $value) {
        $this->fields[$field] = utf8_decode($value);
    }

    public function index() {

        $pedidos = Pedidos::where('user_id', \Auth::user()->id)->get()->toArray();


        foreach ($pedidos as $key => $pedido) {
            $produto = Produtos::where('id', Carrinho::where('pedido', $pedido['id'])->first()['product_id'])->first();
            $pedidos[$key]['img'] = $produto['img'];
        }

        return view('painel.pages.meus-pedidos', compact('pedidos'));
    }

    public function todos() {

        $pedidos = Pedidos::where('status', 'Pago')->get()->toArray();


        foreach ($pedidos as $key => $pedido) {
            $produto = Produtos::where('id', Carrinho::where('pedido', $pedido['id'])->first()['product_id'])->first();
            $pedidos[$key]['img'] = $produto['img'];
        }

        return view('painel.pages.todos-pedidos', compact('pedidos'));
    }

    public function add($endereco, $codigo) {
        $qntdCart = Carrinho::where('user_id', \Auth::user()->id)->where('pedido', 0)->count();
        if ($qntdCart == 0) {
            return redirect('/painel/meus-pedidos')->withErrors(['Carrinho vazio.']);
        }

        $endereco = Enderecos::where('id', $endereco)->first();
        $endereco['cep'] = str_replace('-', '', $endereco['cep']);
        $produtos = Carrinho::carrinhoAtual();
        $produtosCarrinho = $produtos;
        if (!$produtos)
            return redirect("/painel/meus-pedidos");
        // Calcula o subtotal
        $subtotal = 0;
        $pesoTotal = 0;
        $descricao = '';
        $subtotal2 = 0;
        foreach ($produtos as $produto) {

            $quantidade = $produto['quantidade'];
            $preco = $produto['produto']['preco'] * $quantidade;
            if (\Auth::user()->meu_desconto($produto['product_id'])) {
                $subtotal +=\Auth::user()->meu_desconto($produto['product_id']) * $quantidade;
                $descricao.=$produto['produto']['nome'] . '  R$' . \Auth::user()->meu_desconto($produto['product_id']) . ' Quantidade: ' . $quantidade . " \n";
            } else {
                $subtotal+=$preco;
                $descricao.=$produto['produto']['nome'] . '  R$' . $preco . ' Quantidade: ' . $quantidade;
            }
            $subtotal2 += $preco;
            $pesoTotal += $quantidade * $produto['produto']['peso'];
        }

        // Calcula o frete do pedido
        // Calcula o frete do pedido
        if ($_GET['pagamento'] == 1 or $_GET['pagamento'] == 4) {
            $frete = $this->calculaFrete($codigo, $endereco['cep'], $pesoTotal);
        } else {
            $frete = 0;
        }
        $total = $subtotal + $frete;
        // Cria um novo pedido
        if ($_GET['pagamento'] == 1) {
            if (\Auth::user()->saldo >= $total) {
                \Auth::user()->removeSaldo(\Auth::user()->id, $total, 'Compra', 7);
                /* foreach ($produtos as $produto) {
                  \Auth::user()->bonus_compra($produto['product_id']);
                  } */
                $pedido = new Pedidos;
                $pedido->user_id = \Auth::user()->id;
                $pedido->id_endereco = $endereco['id'];
                $pedido->status = "Pago";
                $pedido->preco = $total;
                $pedido->produtos = json_encode($produtos);
                $pedido->date = date("Y-m-d");
                $pedido->save();
                $produ = new Produtos();
                /* foreach ($produtos as $value) {
                  $produ->add_venda($value['product_id'], $value['quantidade']);
                  } */
                \App\extratos::create(['user_id' => \Auth::user()->id, 'data' => date('Y-m-d'), 'descricao' => 'Compra<br>' . $descricao, 'valor' => $total, 'beneficiado' => 1, 'tipo' => 7]);


                $idPay = \DB::table('pagamentos')->insertGetId(['valor' => $total, 'paymentMethod' => 'None', 'status' => 'Pago', 'date' => date('Y-m-d'), 'user_id' => \Auth::user()->id, 'reference' => 'kjkjkj', 'paymentLink' => 'kkkk', 'pacote' => $pedido->id, 'tipo' => 'Compra']);
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime(date('Y-m-d'))));
                $values['user_id'] = \Auth::user()->id;
                $values['status'] = 1;
                $values['validade'] = $validade;
                $values['data'] = date('Y-m-d');
                $values['pagamento_id'] = $idPay;
                $values['data_pagamento'] = date('Y-m-d');
                \DB::table('faturas')->insert($values);


                $carrinhos = Carrinho::where('pedido', 0)->where(
                                'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
                return redirect('/painel/home')
                                ->with('success', 'Parabéns! Sua compra foi efetuada com sucesso! Breve entratermos em contato para enviar informações.Fique atento(a) ao seu email.');
            } else {
                return redirect('/painel/meus-pedidos')->withErrors(['Saldo insuficiente.']);
            }
        } elseif ($_GET['pagamento'] == 4) {
            $tipo = 'Compra';
            $pedido = new Pedidos;
            $pedido->user_id = \Auth::user()->id;
            $pedido->id_endereco = $endereco['id'];
            $pedido->status = "Pendente";
            $pedido->preco = $total;
            $pedido->produtos = json_encode($produtos);
            $pedido->date = date("Y-m-d");
            $pedido->save();
            $reference = 'pedido_' . $pedido->id . '_' . \Auth::user()->id;

            Pedidos::where('id', $pedido->id)->update(['id_pag' => $reference]);

            $carrinhos = Carrinho::where('pedido', 0)->where(
                            'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
            define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
            define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));
            $shop_login = MY_SHOP_LOGIN;
            $shop_secret = MY_SHOP_SECRET;
            $amount = $total;
            $currency = 'BRL';
            $invoice_id = $reference;
            $payment_description = $descricao;
            $signature = '';
            $message = 'S' . $shop_login . $amount . $currency . $invoice_id;
            $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));
            $pacote = $pedido->id;
            $idPay = \DB::table('pagamentos')->insertGetId(['valor' => $amount, 'paymentMethod' => 'Deposito', 'status' => 'Pendente', 'date' => date('Y-m-d'), 'user_id' => \Auth::user()->id, 'reference' => $reference, 'paymentLink' => '', 'pacote' => $pacote, 'tipo' => $tipo]);
            $usr = new User();
            $usr->criarFatura($pacote, \Auth::user()->id, 0, $idPay, 0, 1);
            $carrinhos = Carrinho::where('pedido', 0)->where(
                            'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
            return redirect('/painel/faturas')
                            ->with('success', 'Parabéns! Sua compra fatura foi gerada com sucesso! ');
        } elseif ($_GET['pagamento'] == 3) {
            # informações do cadastrante
            $dados = User::where('id', \Auth::user()->id)->first(); // Trocar nome da session de dados para ??

            $codigo = substr($dados->telefone, 1, 2);
            $telefone = substr($dados->telefone, 4, 10);

            $tipo = 'Compra';
            $pedido = new Pedidos;
            $pedido->user_id = \Auth::user()->id;
            $pedido->id_endereco = $endereco['id'];
            $pedido->status = "Pendente";
            $pedido->preco = $total;
            $pedido->produtos = json_encode($produtos);
            $pedido->date = date("Y-m-d");
            $pedido->save();
            $reference = 'pedido_' . $pedido->id . '_' . \Auth::user()->id;


            $produtos = Carrinho::carrinhoAtual();
            $pedido->produtos = json_encode($produtos);
            Pedidos::where('id', $pedido->id)->update(['id_pag' => $reference]);

            /* $carrinhos = Carrinho::where('pedido', 0)->where(
              'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]); */
            if (!$produtos)
                return redirect("/painel/meus-pedidos");
            $i = 0;
            foreach ($produtosCarrinho as $produt) {
                $this->add_field('itemId' . ($i + 1), $produt['product_id']);
                $this->add_field('itemDescription' . ($i + 1), $produt['produto']['nome']);
                $this->add_field('itemQuantity' . ($i + 1), $produt['quantidade']);
                $this->add_field('itemWeight' . ($i + 1), $produt['produto']['peso'] * 100);
                if (\Auth::user()->meu_desconto($produt['product_id'])) {
                    $amnt = \Auth::user()->meu_desconto($produt['product_id']);

                } else {
                    $amnt = $produto['produto']['preco'];
                }

      


                $this->add_field('itemAmount' . ($i + 1), $amnt);
                $i++;
            }

            \Auth::user()->telefone = str_replace(['(', ')', '-'], '', \Auth::user()->telefone);
            $this->add_field('reference', $reference);
            $this->add_field('senderAreaCode', substr(\Auth::user()->telefone, 1, 2));
            $this->add_field('senderPhone', substr(str_replace('-', '', \Auth::user()->telefone), 2, 8));
            $this->add_field('senderEmail', \Auth::user()->email);
            $this->add_field('senderPhone', substr(str_replace('-', '', \Auth::user()->telefone), 2, 8));
            $this->add_field('senderEmail', \Auth::user()->email);

            $this->add_field('shippingAddressStreet', $endereco->endereco);
            $this->add_field('shippingAddressNumber', intval($endereco->endereco));
            $this->add_field('shippingAddressComplement', $endereco->complemento);
            $this->add_field('shippingAddressDistrict', $endereco->bairro);
            $this->add_field('shippingAddressPostalCode', $endereco->cep);
            $this->add_field('shippingAddressCity', $endereco->cidade);
            $this->add_field('shippingAddressState', $endereco->sigla);
            $this->add_field('shippingAddressCountry', 'BRA');
            $this->add_field('shippingType', 3);
            $this->loadPagseguro();


            $curl = curl_init('https://ws.' . $this->pagSeguroConfig()['url'] . '/v2/checkout');
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            $fields = $this->fields;
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->fields));
            $xml = curl_exec($curl);


            /* Tratando erros */
            if ($xml == 'Unauthorized') /* Gerar Log de erro */
                dd('erro');

            curl_close($curl);

            $xml = simplexml_load_string($xml);

            if (count($xml->error) > 0) {/* Gerar Log de erro */
                $erros = $xml->error;
                foreach ($erros as $value) {
                    return json_encode(['url' => 'error', 'message' => $value->message]);
                }
                return false;
            } else {
                $preferencial = 0;
                $urlPay = 'https://' . $this->pagSeguroConfig()['url'] . '/v2/checkout/' . 'payment.html?code=' . $xml->code;
                /* $carrinhos = Carrinho::where('pedido', 0)->where(
                  'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
                  define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
                  define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));
                  $shop_login = MY_SHOP_LOGIN;
                  $shop_secret = MY_SHOP_SECRET;
                  $amount = $total;
                  $currency = 'BRL';
                  $invoice_id = $reference;
                  $payment_description = $descricao;
                  $signature = '';
                  $message = 'S' . $shop_login . $amount . $currency . $invoice_id;
                  $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret))); */

                $pacote = $pedido->id;
                $idPay = \DB::table('pagamentos')->insertGetId(['valor' => $subtotal, 'paymentMethod' => 'PagSeguro', 'status' => 'Pendente', 'date' => date('Y-m-d'), 'user_id' => \Auth::user()->id, 'reference' => $reference, 'paymentLink' => $urlPay, 'pacote' => $pacote, 'tipo' => $tipo]);
                $usr = new User();
                $usr->criarFatura($pacote, \Auth::user()->id, 0, $idPay, 0, 1);
                if ($urlPay == '') {
                    echo json_encode(['url' => 'error', 'message' => 'Ocorreu um erro.']);
                } else {

                    $carrinhos = Carrinho::where('pedido', 0)->where(
                                    'user_id', \Auth::user()->id)->update(['pedido' => $pedido->id]);
                    echo json_encode(['url' => $urlPay, 'code' => $xml->code]);
                }
            }
        }
    }

    public function pagSeguro($total) {
        $id = mt_rand(1262055681, 1262055681);

        $paymentRequest = new PagSeguroPaymentRequest();

        $paymentRequest->addItem(mt_rand(1262055681, 1262055681), "#Seu Pedido", 1, $total);
        $paymentRequest->setSender(
                \Auth::user()->name, \Auth::user()->email, '', '', \Auth::user()->cpf, '');

        $paymentRequest->setCurrency("BRL");
        $reference = md5(\Auth::user()->name . $id . time());
        $paymentRequest->setReference($reference);
        $paymentRequest->setRedirectUrl(env('PAGSEGURO_REDIRECT'));


        $paymentRequest->addParameter('notificationURL', env('PAGSEGURO_NOTIFICATION'));

        $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()

        $checkoutUrl = $paymentRequest->register($credentials);


        return array('id_pag' => mt_rand(1262055681, 1262055681), 'redirect' => $checkoutUrl);
    }

    // Retorna detalhes de um pedido
    public function pedido($id) {

        $pedido = Pedidos::where('id', $id)->first();

        if (!\Auth::user()->isAdmin()) {
            // Verifica se o pedido é do usuário mesmo
            if ($pedido['user_id'] != \Auth::user()->id) {
                return redirect("/painel/meus-pedidos/");
            }
        }

        // Pega o endereco e os produtos do pedido
        $endereco = Enderecos::where('id', $pedido['id_endereco'])->first();
        $produtos = Carrinho::carrinhoPedido($id);

        return view('painel.pages.pedido', compact('produtos', 'endereco', 'pedido'));
    }

    public function edit($id) {

        $dados = Pedidos::where('id', $id)->first();
        return view('admin.pages.editar-pedido', compact('dados'));
    }

    public function update() {
        $data = \Input::all();
        $valida = [
            'id' => 'integer|required',
            'status' => 'required',
            'info' => 'required',
        ];

        $validator = \Validator::make($data, $valida);
        if ($validator->fails()) {
            return redirect('admin/pedido/edit/' . $data['id'])->withErrors($validator)->withInput();
        } else {
            $use = new User();
            $userInfo = $use->userInfo(Pedidos::where('id', $data['id'])->first()->user_id);

            if ($data['status'] == 'Encaminhado') {
                $dataMail['subject'] = 'Alteração de status!';
                $dataMail['content'] = "<h5>O seu pedido foi encaminhado!</h5>
                    <b>" . $data['info']
                ;
                /* $produtos = json_decode(Pedidos::where('id', $data['id'])->first()->produtos);
                  foreach ($produtos as $value) {
                  for ($i = 1; $i <= $value->quantidade; $i++) {
                  $use->bonus_compra($value->product_id, $userInfo['id']);
                  }
                  //
                  }
                  $produ = new Produtos();
                  foreach ($produtos as $value) {
                  $produ->add_venda($value->product_id, $value->quantidade);
                  } */
                $this->enviarEmail($userInfo['id'], $dataMail['subject'], $dataMail['content']);
            } else {
                $dataMail['subject'] = 'Alteração de status!';
                $dataMail['content'] = "<h5>O seu pedido mudou para o status:" . $data['status'] . " </h5>
                    <b>" . $data['info']
                ;

                $this->enviarEmail($userInfo['id'], $dataMail['subject'], $dataMail['content']);
            }

            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);
            unset($data['_method']);
            \DB::table('pedidos')->where('id', $id)->update($data);
            return redirect('admin/pedido/edit/' . $id)->with('success', 'Sucesso!');
        }
    }

}
