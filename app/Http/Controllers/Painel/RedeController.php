<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Referrals;
use App\User;
use Illuminate\Http\Request;
use App\Binario;

class RedeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        \Auth::user()->produto_pacote(\Auth::user()->id);
        $binario = new Binario();
        if (Referrals::where('system_id', \Auth::user()->id)->count() == 0) {
            return redirect('/painel/')
                            ->withErrors(['Nenhum usuário encontrado']);
        }

        return view('painel.pages.rede', compact('binario'));
    }

    public function redeBinaria() {
        $binario = new Binario();


        return view('painel.pages.rede.rede_binaria', compact('binario'));
    }

    public function redeBinariaInterna($id) {
        $bin = new Binario();
        $filhos = $bin->getFilhosBinario(\Auth::user()->id);

        if (!$filhos) {
            return redirect('/painel/')
                            ->withErrors(['Nenhum usuário encontrado']);
        } else {
            $perm = array_search($id, $filhos);
            if (!is_numeric($perm)) {
                return redirect('/painel/rede-binaria')
                                ->withErrors(['Usuario não encontrado']);
            } else {
                $user_interna = User::where('id', $id)->first();

                if ($user_interna) {
                    return view('painel.pages.rede.rede_binaria_interna', compact('user_interna'));
                } else {
                    return redirect('/painel/rede-binaria')
                                    ->withErrors(['Usuario não encontrado']);
                }
            }
        }
    }

    public function interna($id) {
        $bin = new Binario();
        $filhos = $bin->getFilhos(\Auth::user()->id);
        if (!$filhos) {
            return redirect('/painel/')
                            ->withErrors(['Nenhum usuário encontrado']);
        } else {
            $perm = array_search($id, $filhos);
            if (!is_numeric($perm)) {
                return redirect('/painel/minha-rede')
                                ->withErrors(['Usuario não encontrado']);
            } else {
                $user_interna = User::where('id', $id)->first();
                if ($user_interna) {
                    return view('painel.pages.rede_interna', compact('user_interna'));
                } else {
                    return redirect('/painel/minha-rede')
                                    ->withErrors(['Usuario não encontrado']);
                }
            }
        }
    }

    public function unilevel() {


        $bin = new Binario();
        $id = \Auth::user()->id;

        $rede = \UnilevelHelper::getUsuariosRede($id,1,9999);
        return view('painel.pages.unilevel', compact('rede'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (Referrals::updateDirection($id, \Input::get('direcao'))) {
            return "<div class='alert alert-success'>Usuario ID: <b>$id</b> Atualizado!</div>";
        } else {
            return "<div class='alert alert-danger'>Não foi possivel atualizar o Usuario: <b>$id</b></div>";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
