<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ProdutosExtApiController extends Controller {

    public $porcentagemIndicacao = 0.5;

    private function verficarChave($chave) {
        if ($chave == env('PASS_API')) {
            return true;
        } else {
            return false;
        }
    }

    public function getNovaindicacao(Request $request) {
        if ($request->produto == 1) {
            $produto = 'Classificados';
        } if ($request->produto == 2) {
            $produto = 'Hospedagens e dominios';
        } if ($request->produto == 3) {
            $produto = 'leilão de centavos';
        }
        if (isset($produto) && $this->verficarChave($request->chave)) {
            $paraInserir['produto'] = $produto;
            $paraInserir['user_id'] = $request->user_id;
            $paraInserir['cliente_id'] = $request->cliente_id;

            \App\produtosExterno::insert($paraInserir);
        }
    }

    public function getBonus(Request $request) {
        if ($request->produto == 1) {
            $produto = 'Classificados';
        } if ($request->produto == 2) {
            $produto = 'Hospedagens e dominios';
        } if ($request->produto == 3) {
            $produto = 'leilão de centavos';
        }

        if (isset($produto) && $this->verficarChave($request->chave)) {
            $userModel = new \App\User();
            $pontosModel = new \App\Ponto();
            $userModel->addSaldo($request->user_id, $request->valor * $this->porcentagemIndicacao, 'Indicação(' . $produto . ')');
            $pontosModel->addPpUnico($request->user_id, $request->valor * $this->porcentagemIndicacao);
        }
    }

    public function getIndicador(Request $request) {
        if ($request->produto == 1) {
            $produto = 'Classificados';
        } if ($request->produto == 2) {
            $produto = 'Hospedagens e dominios';
        } if ($request->produto == 3) {
            $produto = 'leilão de centavos';
        }
        if (isset($produto) && $this->verficarChave($request->chave)) {
            $paraInserir['produto'] = $produto;
            $paraInserir['user_id'] = $request->user_id;
            $paraInserir['cliente_id'] = $request->cliente_id;

            $res = \App\produtosExterno::where('cliente_id', $request->cliente_id)->where('produto', $produto);
            $retorno['status'] = false;
            if ($res->count() > 0) {
                $retorno['status'] = true;
                $retorno['user_id'] = $res->first()->user_id;
            }

            echo json_encode($retorno);
        }
    }

    public function getUsarvoucher(Request $request) {
        if ($request->produto == 1) {
            $produto = 'Classificados';
        } if ($request->produto == 2) {
            $produto = 'Hospedagens e dominios';
        } if ($request->produto == 3) {
            $produto = 'leilão de centavos';
        }
        if (isset($produto) && $this->verficarChave($request->chave)) {
            $res = \DB::table("voucher_servicos")->where('status', 0)->where('codigo', $request->code)->where('servico',$produto);
            $retorno['status'] = false;
            if ($res->count() > 0) {
                $res->update(['status' => 1]);
                $retorno['status'] = true;
            }
            echo json_encode($retorno);
        }
    }

    public function getTeste() {
        $url = 'http://republicnetwork.com.dev/products_ext_Y3Jvbl91ZHNncnJyZ3JnZn/indicador?produto=1&chave=123456&cliente_id=987';
        $json = file_get_contents($url);
        $res = json_decode($json, true);
        if ($res['status'] == 1) {

            $url = 'http://republicnetwork.com.dev/products_ext_Y3Jvbl91ZHNncnJyZ3JnZn/bonus?produto=1&chave=123456&valor=20&user_id=' . $res['user_id'];
            $json = file_get_contents($url);
            $res = json_decode($json, true);
        }
    }

}
