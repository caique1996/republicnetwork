<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ProdutosExternos extends Controller {

    /**
     *  Exibe um relatorio de informações sobre os serviços
     *
     *
     * @return void .
     */
    public function getRelatorio() {
        $produtos= \App\produtosExterno::where('user_id', \Auth::user()->id)->get();
        
        $qntdUsersClassificados = \App\produtosExterno::where('produto', 'Classificados')->where('user_id', \Auth::user()->id)->count();
        $qntdUsersLeilao = \App\produtosExterno::where('produto', 'leilão de centavos')->where('user_id', \Auth::user()->id)->count();
        $qntdUsersHospedagem = \App\produtosExterno::where('produto', 'Hospedagens e dominios')->where('user_id', \Auth::user()->id)->count();
        return view('painel.pages.produtosExternos.relatorios', compact('produtos','qntdUsersClassificados', 'qntdUsersHospedagem', 'qntdUsersLeilao'));
    }

  
       public function getIndicacoes() {
        $produtos= \App\produtosExterno::where('user_id', \Auth::user()->id)->get();
                return view('painel.pages.produtosExternos.indicacoes', compact('produtos'));

       }
       public function getVouchers() {
                $vouchers= \DB::table("voucher_servicos")->where('user_id', \Auth::user()->id)->get();
                return view('painel.pages.produtosExternos.vouchers', compact('vouchers'));
       }
}
