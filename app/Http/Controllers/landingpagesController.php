<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\config;
use Validator;
use Symfony\Component\DomCrawler\Form;
use App\graduacoes;
use App\User;
use Mail;

class landingpagesController extends Controller {

    public function view($id) {
        $dados['dados'] = \App\Landingpages::where('id', $id)->where('user_id', \Auth::user()->id)->first();

        return view('painel.pages.landing.edit', $dados);
    }

    public function landig_page($user) {
        $usr = User::where('username', $user)->where('ativo', 1)->first();
        $landing = \App\Landingpages::where('user_id', $usr['id'])->where('status', 1)->first();
        if (isset($usr['id']) and isset($landing['id'])) {
            return view('painel.pages.site-pessoal', compact('usr', 'landing'));
        } else {
            echo 'Usuário não encontrado.';
        }
    }

    public function index() {
        return view('painel.pages.landigpage');
    }

    public function salvar() {
        $data = \Input::all();

        $valida = [
            'id' => 'integer|required',
            'facebook_link' => 'url',
            'twitter_link' => 'url',
            'youtube_video' => 'url|required',
            'email' => 'required|email'
        ];
        if (!empty($data['youtube_video'])) {
            $url = $data['youtube_video'];
            $passa = parse_url($url);
            $dominio = $passa['host'];
            $dominio = str_replace("www.", "", $dominio);
            if ($dominio <> 'youtube.com') {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Esse link não pertence ao youtube.
                 </div>
EOL;
            }
        }
        if (!empty($data['twitter_link'])) {
            $url = $data['twitter_link'];
            $passa = parse_url($url);
            $dominio = $passa['host'];
            $dominio = str_replace("www.", "", $dominio);
            if ($dominio <> 'twitter.com') {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Esse link não pertence ao twitter.
                 </div>
EOL;
            }
        }
        if (!empty($data['facebook_link'])) {
            $url = $data['facebook_link'];
            $passa = parse_url($url);
            $dominio = $passa['host'];
            $dominio = str_replace("www.", "", $dominio);
            if ($dominio <> 'facebook.com') {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Esse link não pertence ao Facebook.
                 </div>
EOL;
            }
            if ($dominio <> 'facebook.com') {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Esse link não pertence ao Facebook.
                 </div>
EOL;
            }
        }



        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];

            $dataLanding['facebook_link'] = $data['facebook_link'];
            $dataLanding['twitter_link'] = $data['twitter_link'];
            $dataLanding['youtube_video'] = $data['youtube_video'];
            $dataLanding['email'] = $data['email'];

            \App\Landingpages::where('id', $id)->where('user_id', \Auth::user()->id)->update($dataLanding);
            return <<<EOL
                 <div class="alert alert-success fade in">
                     LandingPage Atualizada
                 </div>
EOL;
        }
    }

    public function enviar_email($user) {
        $usr = User::where('username', $user)->where('ativo', 1)->first();
        $landing = \App\Landingpages::where('user_id', $usr['id'])->where('status', 1)->first();
        if (isset($usr['id']) and isset($landing['id'])) {
            $data = \Input::all();

            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdPNRsTAAAAAOBmjXQWhV-gSHqo6BUg_awr4492&response=" . $_POST['g-recaptcha-response']);
            $res = json_decode($json);
            $usr = User::where('username', $user)->where('ativo', 1)->first();
            $landing = \App\Landingpages::where('user_id', $usr['id'])->where('status', 1)->first();
            $res->success = 1;
            if ($res->success != 1 or $res->success == '') {
                return <<<EOL
                 failed
EOL;
            } else {
                $valida = [
                    'name' => 'required|max:255',
                    'email' => 'required|email',
                    'message' => 'required'
                ];
                $validator = Validator::make($data, $valida);

                if ($validator->fails()) {
                    echo 'failed';
                } else {
                    $mensagem = '';
                    $mensagem .= "Nome: {$usr['name']} \n";
                    $mensagem .= "Email: {$data['email']} \n";
                    $mensagem .= " {$data['message']}";
                    session(['sendEmail' => $landing['email'], 'sendName' => $usr['name'], 'mensagem' => $mensagem]);


                    Mail::raw($mensagem, function ($message) {
                        $message->from('nao-responda@adsply.com', 'Adsply');
                        $message->to(session('sendEmail'), session('sendName'))->subject('Contato via Landingpage');
                    });
                    echo 'sent';
                }
            }
        } else {
            echo 'failed';
        }
    }

}
