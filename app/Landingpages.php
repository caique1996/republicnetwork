<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landingpages extends Model {

    public $timestamps = false;
    protected $fillable = ['user_id', 'email', 'youtube_video','facebook_link','id','twitter_link','pilar_1_titulo','pilar_2_titulo','pilar_2_conteudo','pilar_3_titulo','pilar_3_conteudo','pilar_4_titulo','pilar_4_conteudo','video_titulo','video_descricao','conferencia_link','nome_exibicao','endereco_exibicao','telefone_exibicao'];

}
