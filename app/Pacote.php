<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacote extends Model {

   // protected $fillable = ['duracao_meses','qntd_app','pontos_consumo','validade', 'nome', 'porcentagem_bin', 'descricao', 'produtos', 'valor', 'status', 'binario', 'porcentagem', 'valorBin', 'valorIndicacao'];

    public function expirados() {
        $hoje = date('Y-m-d');
        $hoje = strtotime($hoje);
        $cotas = \DB::table('cotas')->where('status', 1)->get();
        foreach ($cotas as $value) {
            $vencimento = strtotime($value->validade);
            if ($hoje >= $vencimento) {
                \DB::table('cotas')->where('id', $value->id)->update(['status' => 0]);
            }
        }
    }

}
