<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponto extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'pp', 'pq', 'pt', 'p_plus', 'user_id'];

    /**
     * Verifica se o usuario ja possui um registro na tabela de pontos,caso nao possua,insere.
     *
     * @param  int $user_id
     * @return void
     */
    public function insereRegistroPontos($user_id) {
        $resultado = Ponto::where('user_id', $user_id);
        if ($resultado->count() == 0) {
            $paraInserir['user_id'] = $user_id;
            $paraInserir['pp'] = 0;
            $paraInserir['pq'] = 0;
            $paraInserir['pt'] = 0;
            $paraInserir['p_plus'] = 0;
            Ponto::insert($paraInserir);
        }
    }

//pontos pp
    /**
     * adiciona pontos PP para um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function addPpUnico($user_id, $pontos, $beneficiador = 1) {
        $this->insereRegistroPontos($user_id);
        $resultado = Ponto::where('user_id', $user_id)->increment("pp", $pontos);
        extratos::create(['user_id' => $beneficiador, 'data' => date('Y-m-d'), 'descricao' => 'Pontos PP', 'valor' => $pontos, 'beneficiado' => $user_id, 'tipo' => 16]);
        // $this->addPqUnico($user_id, $pontos);
        $this->addPtUnico($user_id, $pontos, $beneficiador);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * remove pontos PP de um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function removePpUnico($user_id, $pontos) {
        $resultado = Ponto::where('user_id', $user_id)->decrement("pp", $pontos);
        extratos::create(['user_id' => 1, 'data' => date('Y-m-d'), 'descricao' => 'Pontos PP', 'valor' => $pontos * (-1), 'beneficiado' => $user_id, 'tipo' => 16]);
        $this->removePtUnico($user_id, $pontos);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * pepga pontos PP gerados em compras por um usuário.
     *
     * @param  int $user_id
     * @return int
     */
    public function getPpGenerated($user_id) {
        $somaDoMes = \DB::table('pagamentos')->join('faturas', 'pagamentos.id', '=', 'faturas.pagamento_id')->where('faturas.status', 1)->where('faturas.user_id', $user_id)->where('pagamentos.tipo', 'Compra')->where('faturas.data_pagamento', 'like', '%' . date('Y-m') . '%')->sum('valor');
        return Ponto::where('user_id', $user_id)->first()->pp;
    }

    /**
     * pepga pontos PP de um único usuário.
     *
     * @param  int $user_id
     * @return int
     */
    public function getPpUnico($user_id) {
        $this->insereRegistroPontos($user_id);

        return Ponto::where('user_id', $user_id)->first()->pp;
    }

    /**
     * distribui pontos PP para a rede unilevel.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function addPpMulti($user_id, $pontos) {
        $this->insereRegistroPontos($user_id);
        $resultado = Ponto::where('user_id', $user_id)->increment("pp", $pontos);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

//pontos pq
    /**
     * adiciona pontos PQ para um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function addPqUnico($user_id, $pontos) {
        $this->insereRegistroPontos($user_id);
        $resultado = Ponto::where('user_id', $user_id)->increment("pq", $pontos);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * remove pontos PQ de um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function removePqUnico($user_id, $pontos) {

        $resultado = Ponto::where('user_id', $user_id)->decrement("pq", $pontos);
        $this->atualizaPqUnico($user_id);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * atualiza pontos PQ de um único usuário.
     *
     * @param  int $user_id
     * @return void
     */
    public function atualizaPqUnico($user_id) {
        $graduacaoModel = new graduacoes();
        $userModel = new User();
        $proximaGraduacao = $graduacaoModel->proximaGraduacao($user_id);
        $userInfo = User::where('id', $user_id)->first();
        if (!isset($userInfo['id'])) {
            return false;
        }
        if (!$proximaGraduacao) {

            $proximaGraduacao = $userInfo->getGraduacaoAtual();
        }
        $pme = $proximaGraduacao->pme;

        $indicadosDiretos = User::where('pai_id', $user_id);
        if ($indicadosDiretos->count() == 0) {
            return false;
        } else {
            $pq = 0;
            foreach ($indicadosDiretos->get() as $indicado) {
                $limite = 0;
                $ptUser = $this->getPtUnico($indicado->id);
                $limite = $proximaGraduacao->pontuacao * $pme;
                $valor = $ptUser;
                if ($valor > $limite) {
                    $valor = $limite;
                }
                $pq = $pq + $valor;
            }

            Ponto::where('user_id', $user_id)->update(['pq' => $pq]);
        }
    }

    /**
     * pegas os pontos PQ de um único usuário.
     *
     * @param  int $user_id
     * @return int
     */
    public function getPqUnico($user_id) {
        $resultado = Ponto::where('user_id', $user_id);
        if ($resultado->count() > 0) {
            return $resultado->first()->pq;
        } else {
            return 0;
        }
    }

//pontos pt
    /**
     * pegas os pontos PT de um único usuário.
     *
     * @param  int $user_id
     * @return int
     */
    public function getPtUnico($user_id) {
        $resultado = Ponto::where('user_id', $user_id);
        if ($resultado->count() > 0) {
            return $resultado->first()->pt;
        } else {
            return 0;
        }
    }

    /**
     * pega PT da rede de um único usuário.
     *
     * @param  int $user_id
     * @return int
     */
    public function getPtUserTotal($user_id) {
        $bin = new Binario();
        $bin->filhosBin = [];
        \UnilevelHelper::limpaRede();
        $filhos = \UnilevelHelper::getUsuariosRede($user_id);

        if (count($filhos) > 0) {
            $pt = 0;
            foreach ($filhos as $value) {

                $user = $value[0]['userData'];
                $pt = $pt + $this->getPtUnico($user->id);
            }
            return $pt + $this->getPtUnico($user_id);
        } else {
            return 0;
        }
    }

    /**
     * adiciona pontos PT para um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function addPtUnico($user_id, $pontos, $beneficiador = 1) {
        $this->insereRegistroPontos($user_id);
        $resultado = Ponto::where('user_id', $user_id)->increment("pt", $pontos);
        extratos::create(['user_id' => $beneficiador, 'data' => date('Y-m-d'), 'descricao' => 'Pontos PT', 'valor' => $pontos, 'beneficiado' => $user_id, 'tipo' => 17]);

        if ($resultado) {
            $bin = new Binario();
            \UnilevelHelper::limpaRede();
            $superiores = \UnilevelHelper::getUsuariosSuperiores($user_id);
            if (count($superiores) > 0) {
                foreach ($superiores as $user) {
                    $this->atualizaPqUnico($user);
                }
            } else {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * remove pontos PT de um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function removePtUnico($user_id, $pontos) {
        $resultado = Ponto::where('user_id', $user_id)->decrement("pt", $pontos);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

//pontos p_plus
    /**
     * adiciona pontos P_PLUS para um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function addPplusUnico($user_id, $pontos) {
        $this->insereRegistroPontos($user_id);
        $resultado = Ponto::where('user_id', $user_id)->increment("p_plus", $pontos);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * remove pontos P_PLUS de um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function removePplusUnico($user_id, $pontos) {
        $resultado = Ponto::where('user_id', $user_id)->decrement("pt", $pontos);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * zera toda a pontuação de um único usuário.
     *
     * @param  int $user_id
     * @param  int $pontos
     * @return bolean
     */
    public function zeraPontuacao($user_id) {
        $beneficiador = 1;
        extratos::create(['user_id' => $beneficiador, 'data' => date('Y-m-d'), 'descricao' => 'Pontos PP', 'valor' => $this->getPpUnico($user_id) * -1, 'beneficiado' => $user_id, 'tipo' => 16]);
        extratos::create(['user_id' => $beneficiador, 'data' => date('Y-m-d'), 'descricao' => 'Pontos PT', 'valor' => $this->getPtUnico($user_id) * -1, 'beneficiado' => $user_id, 'tipo' => 17]);

        $resultado = Ponto::where('user_id', $user_id)->delete();
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

    //p+
    /**
     * Insere o pq mensal.
     *
     * @param  int $user_id
     * @return true
     */
    public function addPplus($user_id) {
        $pq = $this->getPqUnico($user_id);
        $paraInserir['user_id'] = $user_id;
        $paraInserir['value'] = $pq;
        $paraInserir['date'] = date('Y-m-d');

        $resultado = \DB::table('p_plus')->insert($paraInserir);
        if ($resultado) {
            return true;
        } else {
            return false;
        }
    }

}
