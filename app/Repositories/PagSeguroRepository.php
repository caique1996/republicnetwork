<?php

namespace App\Repositories;

use App\Pagamentos;
use App\User;
use App\extratos;
use Auth;
use GuzzleHttp\Client;
use App\Binario;
use App\Pacote;

class PagSeguroRepository {

    private $client;
    private $clientv3;
    private $token;

    public function __construct() {
        if (env('PAGSEGURO_SANDBOX')) {
            $this->client = new Client([
                'base_uri' => 'https://ws.sandbox.pagseguro.uol.com.br/v2/',
            ]);

            $this->clientv3 = new Client([
                'base_uri' => 'https://ws.sandbox.pagseguro.uol.com.br/v3/',
            ]);

            $this->token = env('PAGSEGURO_TOKEN_SANDBOX');
        } else {
            $this->client = new Client([
                'base_uri' => 'https://ws.pagseguro.uol.com.br/v2/',
            ]);

            $this->clientv3 = new Client([
                'base_uri' => 'https://ws.pagseguro.uol.com.br/v3/',
            ]);

            $this->token = env('PAGSEGURO_TOKEN_PROD');
        }
    }

    public function getIdSessao() {
        $xml = $this->client->post('sessions', [
                    'form_params' => [
                        'email' => env('PAGSEGURO_EMAIL'),
                        'token' => $this->token,
                    ]
                ])->getBody()->getContents();

        return simplexml_load_string($xml)->id;
    }

    public function checkout() {
        $xml = $this->client->post('checkout', [
                    'form_params' => [
                        'email' => env('PAGSEGURO_EMAIL'),
                        'token' => $this->token,
                        "currency" => "BRL",
                        "itemId1" => "1",
                        "itemDescription1" => "Seven Brasil",
                        "itemAmount1" => "1.00",
                        "itemQuantity1" => "1",
                        "notificationURL" => env('PAGSEGURO_NOTIFICATION'),
                        "redirectURL" => env('PAGSEGURO_REDIRECT'),
                        "reference" => "SB#" . Auth::user()->id,
                        "senderName" => Auth::user()->name,
                        "senderCPF" => str_replace(['.', '-'], '', Auth::user()->cpf),
                        "senderAreaCode" => substr(Auth::user()->telefone, 0, 2),
                        "senderPhone" => substr(str_replace('-', '', Auth::user()->telefone), 2, 8),
                        "senderEmail" => Auth::user()->email,
                        "shippingAddressStreet" => Auth::user()->endereco,
                        "shippingAddressNumber" => intval(Auth::user()->endereco),
                        "shippingAddressComplement" => "",
                        "shippingAddressDistrict" => Auth::user()->bairro,
                        //"shippingAddressPostalCode" => "36570000",
                        "shippingAddressCity" => Auth::user()->cidade,
                        //"shippingAddressState" => Auth::user()->estado,
                        "shippingAddressCountry" => "BRA",
                        "shippingType" => "3",
                    ],
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=ISO-8859-1'
                    ]
                ])->getBody()->getContents();

        return simplexml_load_string($xml);
    }

    public function transCartao() {
        $xml = $this->client->post('transactions', [
                    'form_params' => [
                        'email' => env('PAGSEGURO_EMAIL'),
                        'token' => $this->token,
                        "paymentMode" => "default",
                        "paymentMethod" => "creditCard",
                        "receiverEmail" => "joaopedrommn@outlook.com",
                        "currency" => "BRL",
                        //"extraAmount" => "1.00",
                        "itemId1" => "1",
                        "itemDescription1" => "Seven Brasil",
                        "itemAmount1" => "150.00",
                        "itemQuantity1" => "1",
                        "notificationURL" => env('PAGSEGURO_NOTIFICATION'),
                        "reference" => "SB#" . Auth::user()->id,
                        "senderName" => Auth::user()->name,
                        "senderCPF" => str_replace(['.', '-'], '', Auth::user()->cpf),
                        "senderAreaCode" => substr(Auth::user()->telefone, 0, 2),
                        "senderPhone" => substr(str_replace('-', '', Auth::user()->telefone), 2, 8),
                        "senderEmail" => 'c24123351248357585700@sandbox.pagseguro.com.br',
                        "senderHash" => \Input::get('senderHashCard'),
                        "shippingAddressStreet" => "Rua dos Estudantes",
                        "shippingAddressNumber" => "55",
                        "shippingAddressComplement" => "Sala 1",
                        "shippingAddressDistrict" => "Centro",
                        "shippingAddressPostalCode" => "36570000",
                        "shippingAddressCity" => 'VICOSA',
                        "shippingAddressState" => "MG",
                        "shippingAddressCountry" => "BRA",
                        "shippingType" => "3",
                        //"shippingCost" => "1.00",
                        "creditCardToken" => \Input::get('cardToken'),
                        "installmentQuantity" => "1",
                        "installmentValue" => "150.00",
                        "noInterestInstallmentQuantity" => "2",
                        "creditCardHolderName" => \Input::get('nome-cartao'),
                        "creditCardHolderCPF" => \Input::get('cpf-cartao'),
                        "creditCardHolderBirthDate" => "27/10/1987",
                        "creditCardHolderAreaCode" => substr(Auth::user()->telefone, 0, 2),
                        "creditCardHolderPhone" => substr(str_replace('-', '', Auth::user()->telefone), 2, 8),
                        "billingAddressStreet" => "Rua dos Estudantes",
                        "billingAddressNumber" => "55",
                        "billingAddressComplement" => "Sala 1",
                        "billingAddressDistrict" => "Centro",
                        "billingAddressPostalCode" => "36570000",
                        "billingAddressCity" => "VICOSA",
                        "billingAddressState" => "MG",
                        "billingAddressCountry" => "BRA",
                    ],
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=ISO-8859-1'
                    ]
                ])->getBody()->getContents();

        return simplexml_load_string($xml);
    }

    public function transBoleto() {

        $xml = $this->client->post('transactions', [
                    'form_params' => [
                        'email' => env('PAGSEGURO_EMAIL'),
                        'token' => $this->token,
                        "paymentMode" => "default",
                        "paymentMethod" => "boleto",
                        "receiverEmail" => "joaopedrommn@outlook.com",
                        "currency" => "BRL",
                        //"extraAmount" => "1.00",
                        "itemId1" => "1",
                        "itemDescription1" => "Seven Brasil",
                        "itemAmount1" => "150.00",
                        "itemQuantity1" => "1",
                        "notificationURL" => env('PAGSEGURO_NOTIFICATION'),
                        "reference" => "SB#" . Auth::user()->id,
                        "senderName" => Auth::user()->name,
                        "senderCPF" => str_replace(['.', '-'], '', Auth::user()->cpf),
                        "senderAreaCode" => substr(Auth::user()->telefone, 0, 2),
                        "senderPhone" => substr(str_replace('-', '', Auth::user()->telefone), 2, 8),
                        "senderEmail" => 'c24123351248357585700@sandbox.pagseguro.com.br',
                        "senderHash" => \Input::get('senderHash'),
                        "shippingAddressStreet" => 'Rua dos Estudantes',
                        "shippingAddressNumber" => '55',
                        "shippingAddressComplement" => "Sala 1",
                        "shippingAddressDistrict" => 'Centro',
                        "shippingAddressPostalCode" => '36570000',
                        "shippingAddressCity" => 'VICOSA',
                        "shippingAddressState" => 'MG',
                        "shippingAddressCountry" => "BRA",
                        "shippingType" => "3",
                    ],
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=ISO-8859-1'
                    ]
                ])->getBody()->getContents();

        return simplexml_load_string($xml);
    }

    public function notification($code = '') {
        $xml = $this->clientv3->get('transactions/notifications/' . $code . "?email=" . env('PAGSEGURO_EMAIL') . "&token=" . env('PAGSEGURO_TOKEN_SANDBOX'), [
                    'form_params' => [
                        'email' => env('PAGSEGURO_EMAIL'),
                        'token' => $this->token,
                    ],
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=ISO-8859-1'
                    ]
                ])->getBody()->getContents();

        return simplexml_load_string($xml);
    }

    public function transaction($code = '') {
        $xml = $this->clientv3->get('transactions/' . $code . "?email=" . env('PAGSEGURO_EMAIL') . "&token=" . env('PAGSEGURO_TOKEN'), [
                    'form_params' => [
                        'email' => env('PAGSEGURO_EMAIL'),
                        'token' => $this->token,
                    ],
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=ISO-8859-1'
                    ]
                ])->getBody()->getContents();

        return simplexml_load_string($xml);
    }

    public function saveTransaction($trans) {
        $pagamentos = new Pagamentos();
        $pagamentos->user_id = Auth::user()->id;
        $pagamentos->reference = $trans->reference;
        $pagamentos->code = $trans->code;
        $pagamentos->status = $this->getStatus($trans->status);
        $pagamentos->paymentMethod = $this->getPaymentMethod($trans->paymentMethod->type);
        $pagamentos->date = $trans->date;
        $pagamentos->lastEventDate = $trans->lastEventDate;
        $pagamentos->paymentLink = $trans->paymentLink;
        return $pagamentos->save();
    }

    public function updateTransaction($trans) {

        $pagamentos = Pagamentos::where('reference', $trans->reference)->first();
        $dataUser = User::where('id', $pagamentos['user_id'])->first();
        $userPai = User::where('id', $dataUser['pai_id'])->first();
        $today = date("d-m-Y");
        $pacoteData = Pacote::where('id', $userPai['pacote'])->first();


        $porcentagemUsr = $pacoteData['porcentagem'];
        echo '% usr ' . $porcentagemUsr . '<br>';
        $porcentagemAdm = 1 - $pacoteData['porcentagem'];
        echo '% usr ' . $porcentagemAdm . '<br>';

        $valorBeneficiado = ($trans->grossAmount) * ($porcentagemUsr);
        echo 'valorBeneficiado ' . $valorBeneficiado . '<br>';

        $valorBeneficiado2 = ($trans->grossAmount) * ($porcentagemAdm);
        echo 'valorBeneficiado2 ' . $valorBeneficiado2 . '<br>';


        if ($trans->status == "3" && $dataUser['ativo'] == 0 && $pagamentos['status'] != 'Paga') {
            $user = User::find($pagamentos->user_id);
            $user->ativaUser();
        }
        if ($pagamentos['pacote'] != 0 && $trans->status == "3" && $pagamentos['status'] != 'Paga') {
            User::where('id', $pagamentos->user_id)->update(['pacote' => $pagamentos['pacote'], 'pacote_status' => 1]);
            echo '2';
        }
        if ($trans->status == "3" && $pagamentos['status'] != 'Paga') {
            extratos::create(['user_id' => $pagamentos['user_id'], 'data' => $today, 'descricao' => 'Novo pagamento', 'valor' => $valorBeneficiado, 'beneficiado' => $dataUser['pai_id']]);
            $novoSaldo = $valorBeneficiado + $userPai['saldo'];
            User::where('id', $dataUser['pai_id'])->update(['saldo' => $novoSaldo]);
            $beneficiado = Binario::where('user_id', $userPai['id'])->first();
            $pontosBeneficiado = $beneficiado['pontos'] + $valorBeneficiado;
            echo 'pontosBeneficiado ' . $pontosBeneficiado . '<br>';
            echo 'novo  saldo ' . $novoSaldo . '<br>';

            if ($pacoteData['binario'] == 1) {
                Binario::create(['user_id' => $userPai['id'], 'pontos' => $pontosBeneficiado, 'data' => $today]);
                extratos::create(['user_id' => $pagamentos['user_id'], 'data' => $today, 'descricao' => 'Pagamento', 'valor' => $valorBeneficiado2, 'beneficiado' => 1]);

                extratos::create(['user_id' => $pagamentos['user_id'], 'data' => $today, 'descricao' => 'Pontos Binários', 'valor' => $pontosBeneficiado, 'beneficiado' => $dataUser['pai_id']]);
            }
        }

        return Pagamentos::where('reference', $trans->reference)->update([
                    'reference' => $trans->reference,
                    'code' => $trans->code,
                    'status' => $this->getStatus($trans->status),
                    'paymentMethod' => $this->getPaymentMethod($trans->paymentMethod->type),
                    'date' => $trans->date,
                    'lastEventDate' => $trans->lastEventDate,
                    'paymentLink' => $trans->paymentLink,
        ]);
    }

    private function getPaymentMethod($type) {
        switch ($type) {
            case 1:
                $method = 'Cartão de crédito';
                break;
            case 2:
                $method = 'Boleto';
                break;
            case 3:
                $method = 'Débito online (TEF)';
                break;
            case 4:
                $method = 'Saldo PagSeguro';
                break;
            case 5:
                $method = 'Oi Paggo';
                break;
            case 7:
                $method = 'Depósito em conta';
                break;
            default:
                $method = 'Default';
                break;
        }
        return $method;
    }

    private function getStatus($statusCode) {
        switch ($statusCode) {
            case 1:
                $status = 'Aguardando pagamento';
                break;
            case 2:
                $status = 'Em análise';
                break;
            case 3:
                $status = 'Paga';
                break;
            case 4:
                $status = 'Disponível';
                break;
            case 5:
                $status = 'Em disputa';
                break;
            case 6:
                $status = 'Devolvida';
                break;
            case 7:
                $status = 'Cancelada';
                break;
            default:
                $status = 'Undefined';
                break;
        }

        return $status;
    }

    public function functionName($param) {
        
    }

}
