<?php

namespace App;
Use Str;
use Illuminate\Database\Eloquent\Model;

class Saque extends Model {

    protected $fillable = ['valor', 'created_at', 'status', 'user_id', 'data_deposito', 'conta', 'mensagem'];

    public function userName($id) {
        $dat = User::where('id', $id)->first();
        return strlen(Str::words($dat['name'], 1, '')) <= 5 ? Str::words($dat['name'], 2, '') : Str::words($dat['name'], 1, '');
    }

}
