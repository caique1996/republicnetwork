<?php

namespace App;

use Avatar;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Str;
use App\User;
use App\Binario;
use App\extratos;
use App\config;
use Hash;

class User extends Authenticatable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['saldo_equiparacao', 'complemento', 'n_complemento', 'cep', 'pais', 'pontos_consumo', 'dataAtivacao', 'photo', 'carteira_b', 'validade_pacote', 'total_div', 'total_bin_esq', 'total_bin_dir', 'graduacao',
        'name', 'email', 'password', 'cpf', 'endereco', 'cidade', 'sexo', 'bairro',
        'estado', 'nascimento', 'telefone', 'pai_id', 'banco', 'agencia',
        'conta', 'operacao', 'pin', 'admin', 'ativo', 'tipo_conta', 'pago', 'segtitular_nm', 'segtitula_cpf', 'direcao', 'username', 'pacote', 'pacote_status', 'created_at', 'saque', 'binario_direita', 'binario_esquerda'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin() {
        return $this->admin; // this looks for an admin column in your users table
    }

    public function ativado() {
        return $this->ativo;
    }

    public function getShortName() {
        return strlen(Str::words($this->name, 1, '')) <= 5 ? Str::words($this->name, 2, '') : Str::words($this->name, 1, '');
    }

    public function getNascimento() {
        return date('d-m-Y', strtotime($this->nascimento));
    }

    public function memberSince() {
        return date('d F, Y', strtotime($this->created_at));
    }

    public function nIndicados() {
        return Referrals::where('pai_id', $this->id)->count();
    }

    public function nIndicadosDir() {
        $dir['esquerda'] = Referrals::where('pai_id', $this->id)->where('direcao', 'esquerda')->count();
        $dir['direita'] = Referrals::where('pai_id', $this->id)->where('direcao', 'direita')->count();

        return $dir;
    }

    public function getIndicados($id = '') {
        if ($id == '') {
            $id = Auth::user()->id;
        }
        $reffer = Referrals::where('pai_id', $id)->get();

        $users = array();

        foreach ($reffer as $key => $r) {
            $users[$key] = $this->where('id', $r->user_id)->first();
        }

        return $users;
    }

    public function getFilhos($id = null, $count = false) {
        if (!$id) {
            $id = $this->id;
        }

        $reffer = Referrals::where('system_id', $id)->orderBy('direcao', 'ASC')->get();

        $users = array();

        foreach ($reffer as $key => $r) {
            $user = $this->where('id', $r->user_id)->first();
            $user->direcao = $this->getUserDirection($r->user_id);
            $users[$key] = $user;
        }

        if ($count) {
            return count($users);
        }

        return $users;
    }

    public function getUserDirection($user_id) {
        if (isset(Referrals::where('user_id', $user_id)->first()->direcao)) {
            return Referrals::where('user_id', $user_id)->first()->direcao;
        } else {
            return false;
        }
    }

    public function getIndicador($user_id) {
        $refer = Referrals::where('user_id', $user_id)->first();
        $user = $this->where('id', $refer->pai_id)->first();

        return $user->id . " - " . $user->username;
    }

    public function getAvatar() {
        return Avatar::create($this->name)->toBase64();
    }

    public function getSaldo() {
        return number_format($this->saldo, 2, ',', '.');
    }

    public function getUserPai() {
        $pai = $this->where('id', $this->pai_id)->first();
        return $pai ? $pai : new $this;
    }

    public function visitas() {
        return Visitas::where('user_id', $this->id)->distinct()->count();
    }

    public function cVouchers() {
        return Voucher::where('user_id', $this->id)->where('status', 0)->count();
    }

    public function display_children($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;
        $result = \DB::select("SELECT user_id FROM referrals WHERE system_id=$parent");

        foreach ($result as $row) {
            $count += 1 + $this->display_children($row->user_id, $level + 1);
        }
        return $count;
    }

    public function display_children_active($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;
        $result = \DB::select("SELECT user_id FROM referrals WHERE system_id=$parent");

        foreach ($result as $row) {
            $dataUser = $this->userInfo($row->user_id);
            if ($dataUser['ativo'] == 1) {
                $count += 1 + $this->display_children_active($row->user_id, $level + 1);
            }
        }
        return $count;
    }

    public function totalDireita($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='direita'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }
        foreach ($result as $row) {
            if ($level == 0) {
                if ($row->direcao == 'direita') {
                    $count += 1 + $this->totalDireita($row->user_id, $level + 1);
                } else {
                    $count += $this->totalDireita($row->user_id, $level + 1);
                }
            } else {
                $count += 1 + $this->totalDireita($row->user_id, $level + 1);
            }
        }
        return $count;
    }

    public function totalEsquerda($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='esquerda'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }

        foreach ($result as $row) {

            if ($row->direcao == 'esquerda') {
                $count += 1 + $this->totalEsquerda($row->user_id, $level + 1);
            } else {
                $count += $this->totalEsquerda($row->user_id, $level + 1);
            }
        }

        return $count;
    }

    public function ativaUser() {
        $this->ativo = 1;
        return $this->save();
    }

    public function userInfo($val, $by = 'id') {
        $dados = array();
        $dados = User::where($by, $val)->first();
        return $dados;
    }

    public function removeSaldo($id, $valor, $desc = 1, $tipo = '') {
        $today = date("Y-m-d");
        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['saldo'] - $valor;
        if (User::where('id', $id)->update(['saldo' => $novoSaldo])) {
//cria extrato
            extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor) * (-1)), 'beneficiado' => $userInfo['id']]);
        }
    }

    public function removePontosConsumo($id, $valor, $desc = 1) {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['carteira_b'] - $valor;
        if (User::where('id', $id)->update(['carteira_b' => $novoSaldo])) {
//cria extrato
            if ($desc <> 1) {
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor) * (-1)), 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function removePontos($id, $pontos, $dir = 'esquerda') {
        $today = date("Y-m-d");
        ;
        $userInfo = $this->userInfo($id);
        if ($dir == 'esquerda') {
            $novoSaldo = $userInfo['binario_esquerda'] - $pontos;
            $update = User::where('id', $id)->update(['binario_esquerda' => $novoSaldo]);
        } else if ($dir == 'direita') {
            $novoSaldo = $userInfo['binario_direita'] - $pontos;
            $update = User::where('id', $id)->update(['binario_direita' => $novoSaldo]);
        }
    }

    public function addPontos($id, $pontos, $desc, $dir = 'esquerda') {
        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $hoje = date('Y-m-d');
        $userInfo = $this->userInfo($id);
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();
        if ($pontos > 0) {
            //$valorBonus = $pontos * $pacoteInfo['bonus_equiparacao'];
            //$this->addSaldo($id, $valorBonus, 'Bônus de equiparação', 2);
            if ($dir == 'esquerda') {
                $col_pontos = 'binario_esquerda';
                $total_col_pontos = 'total_bin_esq';
            } else if ($dir == 'direita') {
                $col_pontos = 'binario_direita';
                $total_col_pontos = 'total_bin_dir';
            }
            User::where('id', $id)->increment($col_pontos, $pontos);
            User::where('id', $id)->increment($total_col_pontos, $pontos);
            extratos::create(['user_id' => 1, 'data' => $hoje, 'descricao' => 'Pontos', 'valor' => $pontos, 'beneficiado' => $id, 'tipo' => 3]);
        }
    }

    public function addPontosTotal($id, $pontos, $desc, $dir = 'esquerda') {

        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $today = date("Y-m-d");
        $userInfo = $this->userInfo($id);
        if ($this->aptoBonusBinario($id)) {

            if ($userInfo['graduacao'] == '' or $userInfo['graduacao'] < 1) {
                $teto = 1000;
            } else {
                $teto = graduacoes::where('id', $userInfo['graduacao'])->first()['teto'];
            }
            if (($this->pontosRecebidos($userInfo['id']) * $config['binario_valor']) >= $teto) {
                
            } else {
                $bin->logMsg($id . 'add ok' . $pontos, 'qualificado');
                $today = date("Y-m-d");
                $userInfo = $this->userInfo($id);
                if ($dir == 'esquerda') {
                    $novoSaldo = $userInfo['total_bin_esq'] + $pontos;
                    $update = User::where('id', $id)->update(['total_bin_esq' => $novoSaldo]);
//cria extrato
                } else if ($dir == 'direita') {
                    $novoSaldo = $userInfo['total_bin_dir'] + $pontos;
                    $update = User::where('id', $id)->update(['total_bin_dir' => $novoSaldo]);
//cria extrato
                }
            }
        }
    }

    public function requisitosGraduacao($id, $graduacaoId) {
        $usrData = $this->userInfo($id);
        if ($usrData['total_bin_esq'] > $usrData['total_bin_dir']) {
            $pontuacao = $usrData['total_bin_dir'];
        } elseif ($usrData['total_bin_dir'] > $usrData['total_bin_esq']) {
            $pontuacao = $usrData['total_bin_esq'];
        } else {
            $pontuacao = 0;
        }
        if (isset($graduacaoAtual['name'])) {
            $gradPoints = $graduacaoAtual['pontuacao'];
        } else {
            $gradPoints = 0;
        }
        $graduacao = graduacoes::where('id', $graduacaoId)->first();
        if (isset($graduacao['name'])) {
            $gradPoints = $graduacao['pontuacao'];
        } else {
            $gradPoints = 0;
        }
        if (isset($graduacao['id'])) {
            $bin = new Binario();
            $filhos = $bin->getFilhos($id);
            $totalEncontrados = 0;
            if ($graduacao['grad_required'] == 0) {
                $totalEncontrados = Referrals::where('pai_id', $id)->count();
            } else {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    if ($usrData['graduacao'] == $graduacao['grad_required']) {
                        $totalEncontrados = $totalEncontrados + 1;
                    }
                }
            }
            $requisitos['qntd_graduados'] = $graduacao['qntd_required'] - $totalEncontrados;
            if ($requisitos['qntd_graduados'] < 1) {
                $requisitos['qntd_graduados'] = 0;
            }
            $requisitos['pontuacao'] = $graduacao['pontuacao'] - $pontuacao;
            if ($requisitos['pontuacao'] < 1) {
                $requisitos['pontuacao'] = 0;
            }
            $requisitos['nome'] = $graduacao['name'];

            return $requisitos;
        }
    }

    public function verificar_graduacao() {
        $pontosModel = new Ponto();
        $id = $this->id;
        $usrData = $this->userInfo($id);
        $graduacaoAtual = graduacoes::where('id', $usrData['graduacao'])->first();
        $pontuacao = $pontosModel->getPqUnico($id);

        if (isset($graduacaoAtual['pontuacao'])) {
            $nexGraduacao = graduacoes::where('id', '<>', $usrData['graduacao'])->where('status', 1)->where('pontuacao', '>', $graduacaoAtual['pontuacao'])->first();

            if (!isset($nexGraduacao['id'])) {
                return false;
            }
        } else {
            $nexGraduacao = graduacoes::where('status', 1)->orderBy('pontuacao', 'asc')->first();
            if ($nexGraduacao['id'] == $graduacaoAtual['id']) {
                return false;
            }
            if (!isset($nexGraduacao['id'])) {
                return false;
            }
        }
        if ($pontuacao >= $nexGraduacao['pontuacao']) {
            \Auth::user()->update(['graduacao' => $nexGraduacao['id']]);
            return $nexGraduacao;
        } else {
            return false;
        }
    }

    public function pontosRecebidos($id) {
        $config = new config();
        $config = $config->getConfig();
        return extratos::where('beneficiado', $id)->where('descricao', 'Pontos Binários')->where('status', 0)->sum('valor') * $config['binario_valor'];
    }

    public function qualificado($id) {
        return true;
    }

    public function qualificadoBinario($id) {
        
    }

    public function getGraduacaoAtual($user_id = '') {
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $graduacao = graduacoes::where('id', @$this->userInfo($user_id)['graduacao']);
        if ($graduacao->count() > 0) {
            return $graduacao->first();
        } else {
            return false;
        }
    }

    public function minhaGraduacao($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $userData = $this->userInfo($id);

        @$graduacao = graduacoes::where('id', $userData['graduacao'])->first()->name;
        if ($graduacao) {
            return $graduacao;
        } else {
            return 'Sem graduação';
        }
    }

    public function minhaGraduacaoIcone($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $userData = $this->userInfo($id);

        @$graduacao = graduacoes::where('id', $userData['graduacao'])->first()->icone;
        return $graduacao;
    }

    public function verificarPin($pin, $custom = 0) {
        $pin_salvo = \Auth::user()->pin;
        if (Hash::check($pin, $pin_salvo)) {
            
        } else {
            if ($custom == 0) {
                echo 'O pin ' . $pin . ' é inválido';
            } else {
                $erro = 'O pin ' . $pin . ' é inválido';
                echo '<div class="alert alert-danger fade in">' . $erro . '</div>';
            }
            exit();
        }
    }

    public function addDiv($userId, $valor) {
        $pacote->expirados();

        $cota = DB::table('cotas')->where('user_id', $userId)->where('status', 1)->first();
        if (isset($cota->id)) {
            $id = $cota->id;

            $totalDivs = $cota->qntd_divisoes + $valor;
            \DB::table('cotas')->where('id', $cota->id)->update(['qntd_divisoes' => $totalDivs]);
        }
    }

    public function removeDiv($userId, $valor) {
        $pacote = new Pacote();

        $pacote->expirados();
        $cota = \DB::table('cotas')->where('user_id', $userId)->where('status', 1)->first();
        if (isset($cota->id)) {
            $id = $cota->id;

            $totalDivs = $cota->qntd_divisoes - $valor;
            \DB::table('cotas')->where('id', $cota->id)->update(['qntd_divisoes' => $totalDivs]);
        }
    }

    public function totalCotas($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $usr = $this->userInfo($id);
        $pacote = Pacote::where('id', $usr['pacote'])->first();
        if (isset($pacote['id'])) {
            return $pacote['total_cotas'];
        } else {
            return 0;
        }
    }

    public function totalDivs($id) {
        if ($id == '') {
            $id = $this->id;
        }
        $pacote = new Pacote();
        $pacote->expirados();

        $total = \DB::table('cotas')->where('user_id', $id)->where('status', 1)->sum('qntd_divisoes');
        return $total;
    }

    public function criarFatura($pacote, $user, $preferencial = 0, $pagamento = 0, $status = 1, $pedido = '') {
        if ($pedido == '') {
            $pacote = Pacote::where('id', $pacote);
            $pacoteValor = $pacote->first()->valor;

            $data = date('Y-m-d');
            $validade = date('Y-m-d', strtotime('+' . $pacote->first()->duracao_meses . 'month', strtotime($data)));
        } else {
            $data = date('Y-m-d');
            $validade = date('Y-m-d', strtotime('+ 2 days', strtotime($data)));
        }
        $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
        $values['user_id'] = $user;
        $values['status'] = $status;
        $values['validade'] = $validade;
        $values['data'] = $data;
        $values['pagamento_id'] = $pagamento;
        \DB::table('faturas')->insert($values);
    }

    public function addSaldo($id = '', $valor, $desc, $tipo = 1) {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        if ($tipo == 13) {
            $novoSaldo = $userInfo['saldo'] + $valor;
            if (User::where('id', $id)->update(['saldo' => $novoSaldo])) {
//cria extrato
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id'], 'tipo' => $tipo]);
            }
        } else {
            if ($valor > 0) {
                $novoSaldo = $userInfo['saldo'] + $valor;
                if (User::where('id', $id)->update(['saldo' => $novoSaldo])) {
                    if ($tipo == 1 or $tipo == 2 or $tipo == 11 or $tipo == 12) {
                        extratos::create(['user_id' => $userInfo['id'], 'data' => $today, 'descricao' => $desc, 'valor' => $valor * (-1), 'beneficiado' => 1, 'tipo' => $tipo]);
                    }
                    extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id'], 'tipo' => $tipo]);
                }
            }
        }
    }

    public function addPontoConsumo($id, $valor, $desc) {
        $today = date("Y-m-d");
        ;
        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['pontos_consumo'] + $valor;
        if ($userInfo['ativo'] == 1) {
            if (User::where('id', $id)->update(['pontos_consumo' => $novoSaldo])) {
//cria extrato
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function ativarCota($id) {
        $usr = new User();
        $fatura = \DB::table('cotas')->where('id', $id)->where('status', 0);

        if (isset($fatura->first()->user_id)) {
            $dadosUsr = User::where('id', $fatura->first()->user_id)->first();
            $faturaData = $fatura->first();
            $hoje = strtotime(date('Y-m-d'));

            $validade = strtotime($faturaData->validade);
            if ($hoje < $validade) {
                if (isset($faturaData->id)) {
                    $voucher = new Http\Controllers\Admin\VoucherController();
                    $pagamentoInfo = \DB::table('pagamentos')->where('id', $faturaData->pagamento_id)->first();
                    $pacoteInfo = \App\Pacote::where('id', $pagamentoInfo->pacote)->first();
                    if ($voucher->mudarPacote($dadosUsr['id'], $pagamentoInfo->pacote)) {

//ativa
                        $validade = date('Y-m-d', strtotime('+' . $pacoteInfo->validade . 'month', strtotime(date('Y-m-d'))));
                        $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
                        $fatura->update(['status' => 1, 'data' => date('Y-m-d'), 'validade' => $validade]);

                        return true;
                    } else {
                        return false;
                    }
                } else {
                    echo 'Fatura não encontrada';
                    return false;
                }
            } else {
                echo 'Fatura expirada';
                return false;
            }
        } else {
            echo 'Fatura não encontrada';
            return false;
        }
    }

    public function indicacaoIndireta($usr) {
        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $sup = $bin->getPaisLevel($usr);
        $userInfo = $this->userInfo($usr);
        $pacoteData = Pacote::where('id', $userInfo['pacote'])->first();
        $i = 1;

        foreach ($sup as $value) {
            if ($i > 1) {
                $index2 = 'nivel' . ($i - 1);
                $valor = $pacoteData[$index2];
                echo $index2 . '<br>';
                if ($valor > 0) {
                    echo 'user: ' . $value['user'] . '  level: ' . $value['user'] . ' ' . $valor . '<br>';
                    if (isset($value['user']) and ! empty($value['user'])) {
                        $desc = "Bônus de indicação indireta ";
                        $this->addSaldo($value['user'], $valor, $desc);
                    }
                }
            }
            $i++;
        }
    }

    public function removeVirgula($param) {
        $pacoteValor = str_replace('.', '', $param);

        $pacoteValor = str_replace(',', '.', $pacoteValor);
        return $pacoteValor;
    }

    public function vouchersSite($userId) {
        $userData = $this->userInfo($userId);
        $total = Pacote::where('id', $userData['pacote'])->first()['qntd_app'];
        for ($i = 1; $i <= $total; $i++) {
            $code = md5(time() . $userId . rand(0, 999999999));
            \DB::table('vouchers_site')->insert(['user_id' => $userId, 'code' => $code]);
        }
    }

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }

    public function aptoBonusBinario($id, $text = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $hoje = date('Y-m-d');
        $userInfo = $this->userInfo($id);
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();
        if ($pacoteInfo['tipo_pagamento'] == 'Gratuito' or $userInfo['ativo'] == 0 or $userInfo['admin'] == 1) {
            return false;
        } else {
            $bin = New Binario();
            $esquerda = \DB::table("referrals_bin")->where('system_id', $id)->where('direcao', 'esquerda');
            $direita = \DB::table("referrals_bin")->where('system_id', $id)->where('direcao', 'direita');
            $esquerdaQntd = 0;
            $direitaQntd = 0;


            //verifica se tem 2 filhos
            if ($esquerda->count() > 0 and $direita->count() > 0) {
                $userEsq = $this->userInfo($esquerda->first()->user_id);
                $userDir = $this->userInfo($direita->first()->user_id);
                if ($userEsq->ativo == 1 and $userEsq->pai_id = $id) {
                    $esquerdaQntd = $esquerdaQntd + 1;
                }
                if ($userDir->ativo == 1 and $userDir->pai_id = $id) {
                    $direitaQntd = $direitaQntd + 1;
                }
                if ($direitaQntd > 0 and $esquerdaQntd > 0) {
                    return true;
                } else {
                    $bin->filhosBin = [];

                    $filhosEsquerda = $bin->getFilhosBinario($userEsq->id);
                    foreach ($filhosEsquerda as $esqFilhoId) {
                        $esqFilho = [];
                        $filho = [];
                        $esqFilho = $this->userInfo($esqFilhoId);
                        $filho = $esqFilho;
                        if (isset($filho['id'])) {
                            if ($filho['ativo'] == 1 and $filho['pai_id'] == $id) {
                                $esquerdaQntd = $esquerdaQntd + 1;
                            }
                        }
                    }
                    $bin->filhosBin = [];
                    $filhosDireita = $bin->getFilhosBinario($userDir->id);
                    foreach ($filhosDireita as $dirFilhoId) {
                        $dirFilho = [];
                        $filho = [];
                        $dirFilho = $this->userInfo($dirFilhoId);
                        $filho = $dirFilho;
                        if (isset($filho['id'])) {
                            if ($filho['ativo'] == 1 and $filho['pai_id'] == $id) {
                                $direitaQntd = $direitaQntd + 1;
                            }
                        }
                    }
                    if ($direitaQntd > 0 and $esquerdaQntd > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        return false;
    }

    public function apto_bonus($id = '', $text = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $hoje = date('Y-m-d');
        $userInfo = $this->userInfo($id);
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();
        if ($pacoteInfo['tipo_pagamento'] == 'Gratuito' or $userInfo['ativo'] == 0 or $userInfo['admin'] == 1) {
            return false;
        } else {
/// $pedidosMes = Pedidos::where('user_id', $id)->where('status', '<>', 'Pendente')->where('date', 'like', '%' . date('Y-m') . '%')->get();
            $hoje = strtotime($hoje);
            $vencimento = strtotime($userInfo['validade_pacote']);

            $pre_vencimento = date('Y-m-d', strtotime('-10 days', strtotime($userInfo['validade_pacote'])));
            $pos_vencimento = date('Y-m-d', strtotime('+10 days', strtotime($userInfo['validade_pacote'])));
            $somaDoMes = \DB::table('pagamentos')->join('faturas', 'pagamentos.id', '=', 'faturas.pagamento_id')->where('faturas.status', 1)->where('faturas.user_id', $userInfo['id'])->where('pagamentos.tipo', 'Compra')->where('faturas.data_pagamento', 'like', '%' . date('Y-m') . '%')->sum('valor');
            if (strtotime(date('Y-m-d')) >= strtotime($pre_vencimento) and strtotime($pos_vencimento) >= strtotime(date('Y-m-d'))) {

                if ($somaDoMes >= $pacoteInfo['valor_renovacao']) {
                    $this->bonus_indicacao_residual($userInfo['id']);
                    $nova_data = date('Y-m-d', strtotime('+ 35 days', strtotime(date('Y-m-d'))));
                    \Auth::user()->update(['validade_pacote' => $nova_data]);
                    \Session::flash("success", "Parabéns,sua conta foi renovada com sucesso.");
                }
            }

            $hoje = date('Y-m-d');
            $userInfo = $this->userInfo($id);
            $hoje = strtotime($hoje);
            $vencimento = strtotime($userInfo['validade_pacote']);

            if ($text == '') {
                if ($hoje > $vencimento) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if ($hoje > $vencimento) {
                    return 'não';
                } else {
                    return 'sim';
                }
            }
        }
    }

//bônus
    function bonus_indicacao($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $userInfo = $this->userInfo($id);
        $bin = new Binario();
        $pais = \UnilevelHelper::getUsuariosSuperiores($id);

        $i = 1;

        //paga indicação direta
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();


        foreach ($pais as $value) {
            if (config::getConf()['niveis_matriz'] >= $i) {
                $key_pacote = "nivel{$i}";
                $desc = 'Bônus de indicação';
                if ($i == 1) {
                    $desc = 'Bônus de indicação direta';
                }
                $valor = $pacoteInfo[$key_pacote];
                if ($valor > 0) {
                    $this->addSaldo($value, $valor, $desc, 1);
                }
            }
            $i++;
        }
    }

    function bonus_indicacao_residual($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $userInfo = $this->userInfo($id);
        $bin = new Binario();
        $pais = \UnilevelHelper::getUsuariosSuperiores($id);

        $i = 1;

        //paga indicação direta
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();

        foreach ($pais as $value) {
            if (config::getConf()['niveis_matriz'] >= $i) {
                $key_pacote = "residual_nivel{$i}";
                $desc = 'Bônus de indicação residual';
                if ($i == 1) {
                    $desc = 'Bônus de indicação residual direta';
                }
                $valor = $pacoteInfo[$key_pacote];
                if ($valor > 0) {
                    $this->addSaldo($value, $valor, $desc, 1);
                }
            }
            $i++;
        }
    }

    //pontos binários
    function bonus_pontos($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $userInfo = $this->userInfo($id);
        $bin = new Binario();
        $pontosModel = new Ponto();
        $pais = $bin->getPaisBinario($id);
        $i = 1;
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();
        for ($i = 0; $i <= count($pais) - 1; $i++) {
            $key_pacote = "pontos_diretos";
            $valor = $pacoteInfo[$key_pacote];
            if ($valor > 0) {
                if ($i == 0) {
                    $pagador = $userInfo['id'];
                } else {
                    $index = $i - 1;
                    $pagador = $pais[$index];
                }
                $direcaoPagador = $this->getUserDirectionBinario($pagador);
                if ($direcaoPagador) {
                    echo $pagador . ' paga ' . $valor . ' a ' . $pais[$i] . ' na ' . $direcaoPagador . '<br>';
                    $this->addPontos($pais[$i], $valor, 'Pontos', $direcaoPagador);
                    $pontosModel->addPtUnico($pais[$i], $valor);
                }
            }
        }
    }

    function bonus_pontos_residual($id = '') {
        if ($id == '') {
            $id = $this->id;
        }
        $userInfo = $this->userInfo($id);
        $bin = new Binario();
        $pais = $bin->getPaisBinario($id);

        $i = 1;
        $pacoteInfo = Pacote:: where('id', $userInfo['pacote'])->first();
        foreach ($pais as $value) {
            if ($i == 1 and $value == $userInfo['pai_id']) {
                $key_pacote = 'residual_pontos';
            } else {
                $key_pacote = "residual_pontos" . $i;
            }
            $valor = $pacoteInfo[$key_pacote];
            if ($valor > 0) {
// echo 'Id: ' . $value . ' level: ' . $i . ' valor: ' . $valor . ' ' . $this->apto_bonus($value,2) . '<br>';
                $this->addPontos($value, $valor, 'Pontos');
            }

            $i++;
        }
    }

    public function meu_desconto($produto) {
        $dados = Produtos:: where('id', $produto)->first();
        $descontos = json_decode($dados['descontos']);
        $pacote_id = \Auth::user()->pacote;
        if (isset($descontos->$pacote_id) and $this->apto_bonus($this->id)) {
            $money = $descontos->$pacote_id;
            $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
            $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

            $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

            $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
            $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '', $stringWithCommaOrDot);

            return (float) str_replace(',', '.', $removedThousendSeparator);
        } else {
            return 0;
        }
    }

    public function bonus_indicacao_direta($user_id) {
        /* $usuario = $this->userInfo($user_id);
          $pacoteInfo = Pacote::where('id', $usuario['pacote'])->first();
          if (isset($usuario['id']) and $pacoteInfo['id']) {
          $this->addSaldo($usuario['pai_id'], $pacoteInfo['indicacao_direta'], 'Bônus de indicação direta', 11);
          } */
    }

    public function bonus_ativacao_voucher($user_id = '') {

        if ($user_id == '') {
            $user_id = $this->id;
        }
        $id = $user_id;
        $bin = new Binario();
        $pais = $bin->getPais($id);

        $i = 1;
//adiciona pontos para o comprador
//distribui pontos para os demais usuários
        foreach ($pais as $value) {
            if (config::getConf()['niveis_matriz'] >= $i) {

                $valor = 10;
                if ($valor > 0) {
                    \DB::table("users")->where('id', $value)->increment("total_bin_dir", $valor);
                    extratos::create(['user_id' => $user_id, 'data' => date('Y-m-d'), 'descricao' => 'Pontos', 'valor' => $valor, 'beneficiado' => $value, 'tipo' => 3]);
///$this->addPontos($value, $valor, 'Pontos');
                }
            }
            $i++;
        }
    }

    public function bonus_compra($produto_id, $user_id = '') {
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $id = $user_id;
        $pontoModel = new Ponto();
        $produtoInfo = Produtos:: where('id', $produto_id)->first();
        $superiores = \UnilevelHelper::getUsuariosSuperiores($user_id);
        if ($produtoInfo->pontos_diretos > 0) {
            $pontoModel->addPpUnico($user_id, $produtoInfo->pontos_diretos, $user_id);
        }
        foreach ($superiores as $user) {
            if ($produtoInfo->pontos_diretos > 0) {
                $pontoModel->addPtUnico($user, $produtoInfo->pontos_diretos, $user_id);
            }
        }
//adiciona pontos para o comprador

        /*
          //distribui pontos para os demais usuários
          foreach ($pais as $value) {
          if ($i == 1 and $value == $userInfo['pai_id']) {
          $key_pacote = "pontos_diretos";
          } else {
          $key_pacote = "pontos_diretos";
          }
          $valor = $pacoteInfo[$key_pacote];
          if ($valor > 0) {
          // echo 'Id: ' . $value . ' level: ' . $i . ' valor: ' . $valor . ' ' . $this->apto_bonus($value,2) . '<br>';
          $this->addPontos($value, $valor, 'Pontos');
          }

          $i++;
          } */
    }

    public function produto_pacote($user) {
        $userInfo = $this->userInfo($user);

        $pacote = Pacote:: where('id', $userInfo['pacote'])->first();
        if ($pacote['produto_id'] > 0) {
            $produto = Produtos:: where('id', $pacote['produto_id'])->first();
            if (isset($produto['id'])) {
                $carrinho_id = \DB::table('carrinhos')->insertGetId(['user_id' => $userInfo['id'], 'product_id' => $produto['id']]);
                if ($carrinho_id) {


                    $produto->gerarVoucherClass($userInfo['id'], 1);


                    $carrinhos = Carrinho::where('pedido', 0)->where(
                            'user_id', $userInfo['id']);

                    $carrinhos = $carrinhos->select(\DB::raw('COUNT(*) as quantidade', 'product_id'), 'product_id', 'img', 'carrinhos.id', 'nome', 'preco', 'peso')->join('produtos', 'produtos.id', '=', 'carrinhos.product_id')->groupBy('product_id')->get()->toArray();

                    $produto = [];
                    foreach ($carrinhos as $key => $dado) {
                        $array = $dado;
                        $array['produto'] = $dado;

                        $produto[] = $array;
                    }
                    $produtos = $produto;

                    $subtotal = 0;
                    $descricao = '';

                    foreach ($produtos as $produto) {

                        $quantidade = $produto['quantidade'];
                        $preco = $produto['produto']['preco'] * $quantidade;

                        $subtotal+=$preco;
                        $descricao.=$produto['produto']['nome'] . '  R$' . number_format($preco, 2) . ' Quantidade: ' . $quantidade;
                    }
                    if ($produtos) {
                        $endereco = Enderecos::where('user_id', $userInfo['id'])->first();
                        $pedido = new Pedidos;
                        $pedido->user_id = $userInfo['id'];
                        $pedido->id_endereco = $endereco['id'];
                        $pedido->status = "Pago";
                        $pedido->preco = $subtotal;
                        $pedido->produtos = json_encode($produtos);
                        $pedido->date = date("Y-m-d");
                        $pedido->info = 'Entrega pendente,breve lhe enviaremos as informações de entrega.';
                        $pedido->save();
                        extratos::create(['user_id' => $this->id, 'data' => date('Y-m-d'), 'descricao' => 'Compra de kit<br>' . $descricao, 'valor' => $subtotal, 'beneficiado' => 1, 'tipo' => 7]);
                        $carrinhos = Carrinho::where('pedido', 0)->where(
                                        'user_id', $this->id)->update(['pedido' => $pedido->id]);
                    }
                }
            }
        }
    }

//binário
    public function getFilhosBin($id = null, $count = false) {
        if (!$id) {
            $id = $this->id;
        }
        $reffer = \DB::table("referrals_bin")->where('system_id', $id)->orderBy('direcao', 'ASC')->get();
        $users = array();
        foreach ($reffer as $key => $r) {
            $user = $this->where('id', $r->user_id)->first();
            $user->direcao = $this->getUserDirectionBinario($r->user_id);
            $users[$key] = $user;
        }
        if ($count) {
            return count($users);
        }
        return $users;
    }

    public function totalEsquerdaBinario($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals_bin WHERE system_id=$parent AND direcao ='esquerda'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals_bin WHERE system_id=$parent");
        }

        foreach ($result as $row) {

            if ($row->direcao == 'esquerda') {
                $count += 1 + $this->totalEsquerdaBinario($row->user_id, $level + 1);
            } else {
                $count += $this->totalEsquerdaBinario($row->user_id, $level + 1);
            }
        }

        return $count;
    }

    public function totalDireitaBinario($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals_bin WHERE system_id=$parent AND direcao ='direita'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals_bin WHERE system_id=$parent");
        }
        foreach ($result as $row) {
            if ($level == 0) {
                if ($row->direcao == 'direita') {
                    $count += 1 + $this->totalDireitaBinario($row->user_id, $level + 1);
                } else {
                    $count += $this->totalDireitaBinario($row->user_id, $level + 1);
                }
            } else {
                $count += 1 + $this->totalDireitaBinario($row->user_id, $level + 1);
            }
        }
        return $count;
    }

    public function getUserDirectionBinario($user_id) {
        $dir = \DB::table("referrals_bin")->where('user_id', $user_id);
        if ($dir->count() == 1) {
            return \DB::table("referrals_bin")->where('user_id', $user_id)->first()->direcao;
        } else {
            return 'esquerda';
        }
    }

    static function infoAndPack($user_id) {
        $userInfo = User:: where('id', $user_id)->first();
        $pacote = Pacote:: where('id', $userInfo['pacote'])->first();
        return ['info' => $userInfo, 'pack' => $pacote];
    }

    public function infoConsumo($user_id = '') {
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $pontoModel = new Ponto();
        /// $pedidosMes = Pedidos::where('user_id', $id)->where('status', '<>', 'Pendente')->where('date', 'like', '%' . date('Y-m') . '%')->get();
        $userInfo = $this->userInfo($user_id);
        $pacoteInfo = Pacote::where('id', $userInfo['pacote'])->first();

        $hoje = strtotime(date('Y-m-d'));
        $vencimento = strtotime($userInfo['validade_pacote']);

        $pre_vencimento = date('Y-m-d', strtotime('-10 days', strtotime($userInfo['validade_pacote'])));
        $pos_vencimento = date('Y-m-d', strtotime($userInfo['validade_pacote']));
        $somaDoMes = $pontoModel->getPpUnico($user_id);
        $totalNecessario = @$pacoteInfo->valor_renovacao - $somaDoMes;
        $retorno = [];
        if (strtotime(date('Y-m-d')) >= strtotime($pre_vencimento) and strtotime($pos_vencimento) >= strtotime(date('Y-m-d'))) {
            if ($somaDoMes >= $pacoteInfo['valor_renovacao']) {
                /// $this->bonus_indicacao_residual($userInfo['id']);
                /* $nova_data = date('Y-m-d', strtotime('+ ' . $pacoteInfo['duracao_meses'] . ' month', strtotime(date('Y-m-d'))));
                  $userInfo->update(['validade_pacote' => $nova_data]);
                  $this->bonus_indicacao_residual($user_id); */

                $retorno['tipo'] = 'success';
                $retorno['msg'] = 'A renovação da sua conta será efetivada após a distribuição do bônus de equipe.Até lá você continua participando normalmente.';
                $retorno['apto'] = true;
                $retorno['delete'] = false;

                return $retorno;
            } else {
                $retorno['tipo'] = 'danger';
                $retorno['msg'] = "Você precisa de mais " . $totalNecessario . " pontos pessoais para renovar sua conta.Você tem até a seguinte data " . $pos_vencimento;
                $retorno['apto'] = true;
                $retorno['delete'] = true;

                return $retorno;
            }
        }
        $retorno['apto'] = true;
        $retorno['delete'] = false;

        return $retorno;
    }

    public function pontosEquipe($user_id = '') {
        $pontoModel = new Ponto();
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $filhos = \UnilevelHelper::getUsuariosRede($user_id, 10);
        $pontos = 0;
        foreach ($filhos as $value) {
            foreach ($value as $usuario) {
                $usuario = $usuario['userData'];

                $pontos+=$pontoModel->getPpUnico($usuario->id) + $usuario->menorPernaBinaria() * 0.5;
            }
        }
        return $pontos;
    }

    public function getPacote() {
        return Pacote::where('id', $this->pacote)->first();
    }

    public function menorPernaBinaria() {
        if ($this->binario_esquerda >= $this->binario_direita) {
            return $this->binario_direita;
        } else if ($this->binario_direita >= $this->binario_esquerda) {
            return $this->binario_esquerda;
        }
    }

}
