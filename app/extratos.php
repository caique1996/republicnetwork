<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class extratos extends Model {

    protected $fillable = ['user_id', 'data', 'descricao', 'valor', 'beneficiado','tipo'];

    public function userName($id) {
        $dat = User::where('id', $id)->first();
        if ($id == 1) {
            $dat['username'] = 'Administração';
        }
        return $dat['username'];
    }

}
