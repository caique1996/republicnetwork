<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class graduacoes extends Model {

    /**
     * Procura a próxima graduação de um usuário
     *
     * @param  int $user_id
     * @return App\graduacoes
     */
    public function proximaGraduacao($user_id) {
        $userModel = new User();
        $userInfo = $userModel->userInfo($user_id);
        if (isset($userInfo['id'])) {
            $graduacaoAtual = $userInfo['graduacao'];
            $graduacaoAtualInfo = graduacoes::where('id', $graduacaoAtual)->first();
            $graduacaoModel = graduacoes::where('status', 1);
            if (isset($graduacaoAtualInfo)) {
                $proximaGraduacao = $graduacaoModel->where('pontuacao', '>', $graduacaoAtualInfo->pontuacao)->orderBy('pontuacao', 'asc')->first();
                if (isset($proximaGraduacao['id'])) {
                    return $proximaGraduacao;
                } else {
                    return false;
                }
            } else {
                $proximaGraduacao = $graduacaoModel->orderBy('pontuacao', 'asc')->first();
                return $proximaGraduacao;
            }
        } else {
            return false;
        }
    }

}
