<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produtosExterno extends Model {

    /**
     *  Pega os dados dos produtos de um usuário
     *
     * @param int $user_id
     * @return void .
     */
    public static function getProductsByUser($user) {
        return Produtos::where('user_id', $user)->get();
    }
  public function getCliente() {
        $url = env('URL_CLASSIFICADOS') . 'userinfo/' . $this->cliente_id;
        $res = file_get_contents($url);
        return json_decode($res);
    }
}
