<?php

return [
    'sandbox' => TRUE,//DEFINI SE SERÁ UTILIZADO O AMBIENTE DE TESTES
    
    'credentials' => [//SETA AS CREDENCIAIS DE SUA LOJA
        'email' => 'caiquemarcelinosouza@ymail.com',
        'token' => '1EC627ADE2474DA79FA5E29B7C3C3863',
    ],
    'currency' => [//MOEDA QUE SERÁ UTILIZADA COMO MEIO DE PAGAMENTO
        'type' => 'BRL'
    ],
    'reference' => [//CRIAR UMA REFERENCIA PARA OS PRODUTOS VENDIDOS
        'idReference' => NULL
    ],
    'proxy' => [//CONFIGURAÇÃO PARA PROXY
        'user'     => NULL,
        'password' => NULL,
        'url'      => NULL,
        'port'     => NULL,
        'protocol' => NULL
    ],
    'url' => 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout',
];
