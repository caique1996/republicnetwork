<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {

    $faker->addProvider(new \App\Faker\Pessoa($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\Address($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\PhoneNumber($faker));

    $lados = ['esquerda', 'direita'];
    $key = array_rand($lados);

    return [
        'name' => trim($faker->name),
        'email' => strtolower($faker->email),
        'username' => $faker->userName,
        'password' => bcrypt('123456'),
        //'remember_token' => str_random(10),
        'cpf' => $faker->cpf(true),
        'endereco' => $faker->streetAddress,
        'cidade' => $faker->city,
        'bairro' => $faker->streetName,
        'estado' => $faker->state,
        'nascimento' => $faker->date('Y-m-d'),
        'telefone' => $faker->phoneNumber,
        'pai_id' => NULL,
        'admin' => 0,
        'ativo' => 0,
        'pago' => 1,
        'direcao' => $lados[$key],
    ];

});
