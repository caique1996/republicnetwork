<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtratosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('extratos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('beneficiado');
            $table->string('valor');
            $table->string('data');
            $table->string('descricao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('extratos');
    }

}
