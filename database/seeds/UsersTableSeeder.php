<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Usuario Inicial',
            'email' => 'example@example.com',
            'username' => 'admin',
            'password' => bcrypt('123456'),
            //'remember_token' => str_random(10),
            'cpf' => '717.419.067-76',
            'endereco' => 'Largo Aaron, 5',
            'bairro' => 'Niteroi',
            'cidade' => 'Demian do Sul',
            'estado' => 'Sergipe',
            'nascimento' => '1983-06-23',
            'telefone' => '(54) 2180-8433',
            'pai_id' => NULL,
            'admin' => 1,
            'ativo' => 1,
            'pago' => 1,
        ]);

        $reffer = new \App\Referrals();
        $reffer->user_id = 1;
        $reffer->pai_id = 0;
        $reffer->system_id = 0;
        $reffer->save();


//        for ($i = 1; $i < 16; $i++) {
//            foreach (factory(App\User::class, 2)->create(['pai_id' => $i]) as $fac) {
//                $reffer = new \App\Referrals();
//                $reffer->user_id = $fac->id;
//                $reffer->pai_id = 1;
//                $reffer->system_id = $i;
//                $reffer->direcao = $fac->direcao;
//                $reffer->save();
//            }
//        }

    }
}
