<?php

use App\Binario;

//Route::resource('teste','PacoteController@index');
// FRONT ROUTES
Route::get('/', function () {
    return view('site.layout');
});
Route::get('/lang/{name}', 'LocaleController@setLocale');

Route::get('language','HomeController@language');

Route::get('/retorno/{retorno}', 'Painel\PagamentoController@retorno');



Route::get('/teste', 'Admin\VoucherController@teste');

Route::get('/pacote/expirados', 'Admin\PacoteController@expirados');

Route::post('/notificacao', 'Painel\PagamentoController@notificacao');
Route::get('/notificacao', 'Painel\PagamentoController@notificacao');

Route::post('/pagamento', 'Painel\PagamentoController@hue');
Route::get('/pagamento', 'Painel\PagamentoController@hue');

Route::post('/notificacao/{metodo}', 'Painel\PagamentoController@notificacao');
Route::get('/notificacao/{metodo}', 'Painel\PagamentoController@notificacao');






Route::get('/sala', function() {
    \App\VisitasSala::create(['ip' => Request::ip()]);
    return redirect('http://www.gvolive.com/conference,sevenoficial');
});

// USER PANEL ROUTES
Route::get('/painel', function () {
    return redirect('/painel/home');
});

Route::get('/cadastro', function () {
    return redirect('/');
});
Route::get('/site', function () {
    return redirect('/');
});
Route::group(['middleware' => 'web'], function () {
    Route::get('/site/{usuario}', 'landingpagesController@landig_page');
    Route::post('/site/enviar_email/{usuario}', 'landingpagesController@enviar_email');
});
Route::group(['middleware' => 'web'], function () {
    Route::get('/cadastro/{indicacao}', 'CadastroController@index');
    Route::post('/cadastro/{indicacao}', 'CadastroController@store');
});

Route::get('localizacao/cidade', 'Painel\UserController@getCidade');
Route::get('localizacao/estado', 'Painel\UserController@getEstados');


Route::group(['middleware' => ['web', 'auth.user'], 'prefix' => 'painel'], function () {

    Route::get('inativo', 'Painel\GNetController@index');
    Route::get('inativo/boleto', 'Painel\GNetController@gerarBoleto');
    Route::post('inativo/cartao', 'Painel\GNetController@pagarCartao');
    Route::get('inativo/testes', 'Painel\GNetController@testes');

    Route::post('ativar-conta', 'Painel\UserController@ativarConta');

    Route::get('meus-dados', 'Painel\UserController@index');
    Route::post('meus-dados', 'Painel\UserController@update');
    Route::get('/ver_fatura', function () {

        if (Auth::user()->id) {
            $pay = new \App\Http\Controllers\Painel\PagamentoController();

            if (@$_GET['novopagamento'] == 1 and ( $_GET['metodo'] == 3)) {
                if ($_GET['tipo'] == 1) {
                    $tipo = 'Ativação de pacote';
                } if ($_GET['tipo'] == 2) {
                    $tipo = 'Upgrade';
                } else {
                    $tipo = 'Ativação de pacote';
                }
                $res = $pay->gerar_link($_GET['metodo'], $_GET['pacote'], Auth::user()->id, $tipo);
                if ($res) {
                    echo $res;
                }
            }
        }
    });
});

Route::group(['middleware' => ['web', 'auth.user', 'ativo'], 'prefix' => 'painel'], function () {
    Route::get('home', function () {
        $binario = new Binario();
        $binario['esquerda'] = $binario->totalEsquerda(\Auth::user()->id);
        $binario['direita'] = $binario->totalDireita(\Auth::user()->id);
        $pacoteCurr = App\Pacote::where('id', \Auth::user()->pacote)->first();
        return view('painel.pages.home', compact('pacoteCurr', 'binario'));
    });
    Route::get('pontos', function () {

        return view('painel.pages.pontos');
    });
    Route::get('upgrade', 'Painel\UpgradeController@index');

    Route::get('graduacoes/', 'graduacoesController@index2');
    Route::get('saque/', 'SaquesController@solicitaSaque');
    Route::get('saques/', 'SaquesController@indexUser');
    Route::get('upgrade', 'Painel\UpgradeController@index');
    Route::post('upgrade', 'Painel\UpgradeController@index');

    Route::get('minha-rede', 'Painel\RedeController@index');
    Route::post('minha-rede/busca', 'Painel\UserController@buscar');

    Route::get('minha-rede/{id}', 'Painel\RedeController@interna');
    Route::post('minha-rede/update/{id}', 'Painel\RedeController@update');
    Route::get('unilevel', 'Painel\RedeController@unilevel');

    // REDE TESTE
    Route::get('teste/minha-rede', 'Painel\RedeTesteController@index');
    Route::post('teste/minha-rede/busca', 'Painel\UserController@buscar');

    Route::get('teste/minha-rede/{id}', 'Painel\RedeTesteController@interna');
    Route::post('teste/minha-rede/update/{id}', 'Painel\RedeTesteController@update');

    Route::get('novaChave', 'Painel\UserController@novachave');
    Route::post('novaChave', 'Painel\UserController@novachave');

    Route::post('suporte/ajax', 'Painel\SuporteController@storeAjax');

    Route::get('vouchers', function () {
        return view('painel.pages.vouchers');
    });

    Route::get('meus-indicados', function () {
        return view('painel.pages.directs');
    });

    Route::resource('suporte', 'Painel\SuporteController');
    Route::resource('materiais', 'Painel\MateriaisController');

    Route::get('transacoes', 'Painel\ExtratosController@index');
    Route::post('transacoes', 'Painel\ExtratosController@index');

    //landinpage
    Route::get('/landingpage/', 'landingpagesController@index');
    Route::get('landingpage/{d}', 'landingpagesController@view');

    Route::post('landingpage/salvar', 'landingpagesController@salvar');


    // Usuario
    Route::get('muda_lado', 'Painel\UserController@muda_lado');
    Route::post('muda_lado', 'Painel\UserController@muda_lado');
    // Usuario
    Route::get('pin', 'Painel\UserController@pin');
    Route::post('pin', 'Painel\UserController@pin');
    Route::get('converter_saldo', 'Painel\UserController@converter_saldo');
    Route::post('converter_saldo', 'Painel\UserController@converter_saldo');
    Route::get('ver_user', 'Painel\UserController@ver_user');
    Route::get('ver_user_transferencia', 'Painel\UserController@ver_user_transferencia');

    Route::get('ativar_user', 'Painel\UserController@ativar_user');
    //mudar foto
    Route::post('mudar_foto', 'Painel\UserController@mudar_foto');
    Route::get('faturas', 'Admin\PacoteController@faturas');
    Route::get('fatura/{id}', 'Admin\PacoteController@excluir_fatura');

//transferir saldo
    Route::get('transferir_saldo', 'Painel\UserController@transferir_saldo');
    Route::post('transferir_saldo', 'Painel\UserController@transferir_saldo');

    Route::post('pagar_saldo', 'Painel\UserController@pagar_fatura');
    Route::get('pagar_saldo', 'Painel\UserController@pagar_fatura');
    //anuncios
    Route::get('criar_anuncio', 'anunciosController@criarAnuncio');
    Route::get('meus-anuncios', 'anunciosController@meus_anuncios');
    Route::get('anuncio/{d}', 'anunciosController@view2');
    Route::post('anuncio/salvar', 'anunciosController@salvar2');
    Route::get('/visitar-anuncios', function () {
        return view('painel.pages.para-visitar');
    });
    Route::get('anuncio/visualizar/{id}', 'anunciosController@visualizar_site');
    Route::get('validar_visualizacao', 'anunciosController@validarVisualizacao');
    Route::get('validar_visualizacao2', 'anunciosController@validarVisualizacao2');
    //loja
    Route::get('produtos', 'Painel\ProdutosController@index');

    Route::get('meu-carrinho', 'Painel\MeuCarrinhoController@index');
    Route::get('meu-carrinho/endereco/', 'Painel\EnderecoController@index');

    Route::get('todos-pedidos', 'Painel\MeusPedidosController@todos');

    Route::get('meus-pedidos', 'Painel\MeusPedidosController@index');
    Route::get('meus-pedidos/pedido/{pedido}', 'Painel\MeusPedidosController@pedido');
    Route::get('meus-pedidos/add/{endereco}/{codigo}', 'Painel\MeusPedidosController@add');

    Route::post('meu-carrinho/endereco/add', 'Painel\EnderecoController@add');

    Route::get('meu-carrinho/add/{product_id}/', 'Painel\MeuCarrinhoController@add');
    Route::get('meu-carrinho/remove/{product_id}/{max?}/', 'Painel\MeuCarrinhoController@remove');
    Route::get('meu-carrinho/qtd/{product_id}/{max?}/', 'Painel\MeuCarrinhoController@qtd');
    Route::get('app-vouchers', function () {
        if (isset($_GET['code'])) {
            $voucher = DB::table('vouchers_site')->where('code', $_GET['code']);
            if ($voucher->count() > 0) {
                $dbConf = array(
                    'driver' => 'mysql',
                    'host' => 'localhost',
                    'database' => 'apps',
                    'username' => 'root',
                    'password' => '6fyFdDzhFq',
                    'charset' => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix' => '',);
                Config::set("database.connections.test", $dbConf);
                $vou = DB::connection('test')->table('admin')->where('lax_id', Auth::user()->id);
                $qntd = $vou->first()->qntd_app;
                $vou->update(['qntd_app' => $qntd + 1]);
                DB::table('vouchers_site')->where('code', $_GET['code'])->delete();
                return redirect(env('BUILDER_APP'));
            } else {
                Session::flash('error', 'Voucher não encontrado.');
            }
        } else {
            return view('painel.pages.app-vouchers');
        }
    });
    Route::post('app-vouchers', function () {

        return view('painel.pages.app-vouchers');
    });
});

Route::group(['middleware' => ['web'], 'prefix' => 'painel'], function () {

    // Authentication Routes...
    Route::get('login', 'Auth\AuthController@showLoginUser');
    Route::post('login', 'Auth\AuthController@login_user');
    Route::get('logout', 'Auth\AuthController@logout');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetFormUser');
    Route::post('password/email', 'Auth\PasswordController@postEmailUser');
    Route::post('password/reset', 'Auth\PasswordController@reset');
});

// ADMIN PANEL ROUTES
Route::get('/admin', function () {
    return redirect('/admin/home');
});

Route::group(['middleware' => ['web'], 'prefix' => 'admin'], function () {
    // Authentication Routes...
    Route::get('login', 'Auth\AuthController@showLoginForm');
    Route::post('login', 'Auth\AuthController@login_admin');
    Route::get('logout', 'Auth\AuthController@logout');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\PasswordController@reset');
});

Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function () {

    Route::get('home', function () {
        return view('admin.pages.relatorios');
    });

    Route::get('produtos/add', 'Painel\ProdutosController@create');
    Route::post('produtos/add', 'Painel\ProdutosController@createPost');

    Route::get('meus-dados', 'Painel\UserController@index');
    Route::post('meus-dados', 'Painel\UserController@update');
    Route::post('rodar-binario', 'BinarioController@index');
    Route::get('rodar-binario', 'BinarioController@index');
    // Materiais
    Route::resource('materiais', 'Admin\MateriaisController');
    // Materiais
    // Usuario
    Route::get('config', 'configController@index');
    Route::post('config', 'configController@update');

    // Vouchers
    Route::get('vouchers', 'Admin\VoucherController@index');
    Route::get('vouchers/adicionar/ {
        user
    }', 'Admin\VoucherController@create');
    Route::post('vouchers/salvar/ {
        user
    }', 'Admin\VoucherController@store');
    Route::post('vouchers/remover/ {
        user
    }', 'Admin\VoucherController@destroy');
    Route::get('vouchers/remover/ {
        user
    }', 'Admin\VoucherController@remover');

    //usuarios manage
    Route::get('usuarios', 'Painel\UserController@manage_usr');


    Route::get('divisaoLucro/', 'BinarioController@divideLucro');
    Route::post('divisaoLucro/', 'BinarioController@divideLucro');
    Route::get('ativarUsr/', 'Admin\VoucherController@ativarUsr');
    Route::post('ativarUsr/', 'Admin\VoucherController@ativarUsr');

    Route::get('statusSaque/', 'Admin\VoucherController@statusSaque');
    Route::post('statusSaque/', 'Admin\VoucherController@statusSaque');

    Route::get('saque/', 'SaquesController@index');
    Route::post('saque/', 'SaquesController@store');
    //usuario
    Route::get('usuario/{d}', 'Painel\UserController@view');
    Route::post('usuario/salvar', 'Painel\UserController@salvar');

    //pacote
    Route::post('pacote/', 'Admin\PacoteController@store');

    Route::post('pacote/create', 'Admin\PacoteController@create');
    Route::get('pacote/create', 'Admin\PacoteController@create');

    Route::get('pacote', 'Admin\PacoteController@index');
    Route::get('pacote/{d}', 'Admin\PacoteController@view');
    Route::post('pacote/salvar', 'Admin\PacoteController@salvar');
    //pacote views
    Route::post('pacote_visualizacoes/', 'Admin\pacoteViewsController@store');
    Route::post('pacote_visualizacoes/create', 'Admin\pacoteViewsController@create');
    Route::get('pacote_visualizacoes/create', 'Admin\pacoteViewsController@create');
    Route::get('pacote_visualizacoes', 'Admin\pacoteViewsController@index');
    Route::get('pacote_visualizacoes/{d}', 'Admin\pacoteViewsController@view');
    Route::post('pacote_visualizacoes/salvar', 'Admin\pacoteViewsController@salvar');




    //trades
    Route::get('trade/', 'Admin\TradesController@index');
    Route::get('trade/create', 'Admin\TradesController@create');
    Route::post('trade/', 'Admin\TradesController@store');

    Route::get('trade', 'Admin\TradesController@index');
    Route::get('trade/{d}', 'Admin\TradesController@view');
    Route::post('trade/salvar', 'Admin\TradesController@salvar');

    Route::get('trade/{d}', 'Admin\TradesController@view');






    Route::get('mudarPacote/', 'Admin\VoucherController@mudarPacote');
    Route::post('mudarPacote/', 'Admin\VoucherController@mudarPacote');

    //graduações
    Route::get('graduacao/create', 'graduacoesController@create');
    Route::get('graduacoes', 'graduacoesController@index');
    Route::post('graduacoes', 'graduacoesController@store');
    Route::get('graduacao/{d}', 'graduacoesController@view');
    Route::post('graduacao/salvar', 'graduacoesController@salvar');
    //adicionar saldo
    Route::get('adicionarSaldo/', 'Painel\UserController@addSaldo');
    Route::post('adicionarSaldo/', 'Painel\UserController@addSaldo');

    //relatorios
    Route::get('relatorios', 'Painel\ExtratosController@relatorios');
    Route::post('relatorios', 'Painel\ExtratosController@relatorios');
    //relatorios
    //faturas
    Route::get('faturas', 'Admin\PacoteController@faturas_admin');
    Route::get('fatura/liberar/{id}', 'Admin\PacoteController@liberar_fatura');
    //anuncios
    Route::get('anuncios', function () {
        return view('admin.pages.anuncios');
    });
    Route::get('anuncio/{d}', 'anunciosController@view');
    Route::post('anuncio/salvar', 'anunciosController@salvar');
});
