
ALTER TABLE  `enderecos` ADD  `siglaEstado` VARCHAR( 20 ) NOT NULL ,
ADD  `complemento` VARCHAR( 255 ) NOT NULL ,
ADD  `numero` INT( 255 ) NOT NULL ;
ALTER TABLE  `enderecos` ADD  `bairro` INT NOT NULL ;

ALTER TABLE  `pagamentos` ADD  `valor` DOUBLE( 10, 2 ) NOT NULL ;

ALTER TABLE  `faturas` ADD  `data_pagamento` DATE NOT NULL ;

