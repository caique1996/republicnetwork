<form class="formAjax" method="post" action="{{url('/admin/pacote/salvar')}}">
    {{ csrf_field() }}
    <input type="hidden" class="form-control" value="<?= $dados['id'] ?>"name="id" />

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Pacote <?= $dados['nome'] ?></h4>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" value="<?= $dados['nome'] ?>" name="nome" />
        </div>
        <div class="form-group">
            <label>Valor</label>
            <input type="text" step="any" class="form-control money" required placeholder="Valor" value="<?= $dados['valor'] ?>" name="valor" id='valor' />
        </div>
        <div class="form-group">
            <label>Valor renovação em pontos</label>
            <input type="text"  class="form-control" required placeholder="Valor renovação em pontos" name="valor_renovacao" value="<?= $dados['valor_renovacao'] ?>" id='valor_renovacao' />
        </div>

        <div class="form-group">
            <label>Bônus de equiparação(Porcentagem em números decimais)</label>
            <input type="number" step="any" class="form-control percent" required placeholder="Porcentagem do bônus " name="bonus_equiparacao" value="<?= $dados['bonus_equiparacao'] ?>" id='bonus_equiparacao' />
        </div>
        <div class="form-group">
            <label>Id do produto/kit</label>
            <input type="number" class="form-control" required placeholder="Id do produto/kit" name="produto_id" value="<?= $dados['produto_id'] ?>" id='produto_id' />
        </div>
        <div class="form-group">
            <label>Descrição </label>
            <textarea class="form-control" required placeholder="Descrição" name="descricao">
                <?= $dados['descricao'] ?>
            </textarea>
        </div>
        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="<?= $dados['status'] ?>"><?= Auth::user()->getStatus($dados['status']) ?></option>

                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Duração </label>
            <select class="form-control" name="duracao_meses">
                <option value='<?= $dados['duracao_meses'] ?>'><?= $dados['duracao_meses'] ?> mês(es)</option>";

                <?php
                for ($i = 1; $i <= 72; $i++) {
                    echo "<option value='$i'>$i mês(es)</option>";
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Tipo de pacote </label>
            <select class="form-control" name="tipo_pagamento">
                <option value="<?= $dados['tipo_pagamento'] ?>"><?= Auth::user()->getStatus($dados['tipo_pagamento']) ?></option>

                <option value="Pago">Pago</option>
                <option value=Gratuito>Gratuito</option>
            </select>
        </div>

        <?php
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            echo '<div class = "form-group">';
            echo"<label>Valor da indicação: nível  {$i}</label>";
            $key = "nivel{$i}";
            $value = $dados[$key];
            echo "<input  type='text' step='any' value='$value' class = 'form-control money'  placeholder = '{$i}º Nível' name ='nivel{$i}'/>
            </div>";
        }
        ?>

        <?php
        for ($i = 1; $i <= env('NIVEIS'); $i++) {
            echo '<div class = "form-group">';
            echo"<label>Valor da indicação residual: nível  {$i}</label>";
            $key = "residual_nivel{$i}";
            $value = $dados[$key];
            echo "<input  type='text' step='any' value='$value' class = 'form-control money'  placeholder = '{$i}º Nível' name ='residual_nivel{$i}'/>
            </div>";
        }
        ?>
        <div class="form-group">
            <label>Valor da pontuação binária</label>
            <input type="number" class="form-control" required placeholder="Valor da indicação pontuação " name="pontos_diretos" value="<?= $dados['pontos_diretos'] ?>" id='pontos_diretos' />
        </div>

        <div class="form-group">
            <label>Valor da pontuação binária residual </label>
            <input type="number" class="form-control" required placeholder="Valor da pontuação residual  " name="pontuacao_re_direta" value="<?= $dados['pontuacao_re_direta'] ?>" id='pontuacao_re_direta' />
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
</form>
<script>
    ///$(".percent").mask("9?9%");
    $('.money').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });

    $(function () {
        /// $(".percent").mask("9?9%");

        /*$(".percent").on("blur", function () {
         var value = ($(this).val().length == 1) ? $(this).val() + '%' : $(this).val();
         $(this).val(value);
         })*/
    });

</script>
