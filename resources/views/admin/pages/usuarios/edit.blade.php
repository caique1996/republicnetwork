<form class="formAjax" method="post" action="{{url('/admin/usuario/salvar')}}">

    {{ csrf_field() }}
    <?php

    // print_r($dados);

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Usuário</h4>

    </div>
    <div class="modal-body">

        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="name" value="{{$dados['name']}}" />
        </div>

        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required placeholder="" name="email" value="{{$dados['email']}}" />
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control"  placeholder="" disabled="" value="{{$dados['username']}}" />
        </div>
        <div class="form-group">
            <label>Administrador </label>
            <select class="form-control" name="admin" onchange="alert('O usuário terá acesso total ao sistema')">
                <option value="{{$dados['admin']}}" selected="">{{getStatus($dados['admin'],1)}}</option>
                <option value="0">Sim</option>
                <option value="1">Não</option>

            </select>
        </div>
        <div class="form-group">
            <label>Pago </label>
            <select class="form-control" name="pago">
                <option value="{{$dados['pago']}}" selected="">{{getStatus($dados['pago'],1)}}</option>
                <option value="0">Sim</option>
                <option value="1">Não</option>
            </select>
            <p>*Não gera bonificação</p>
        </div>
        <div class="form-group">
            <label>Ativo </label>
            <select class="form-control" name="ativo">
                <option value="{{$dados['ativo']}}" selected="">{{getStatus($dados['ativo'],1)}}</option>
                <option value="0">Sim</option>
                <option value="1">Não</option>
            </select>
            <p>*Não gera bonificação</p>

        </div>
        <?php if (env('md_JqHVpdRvx4YV5mxd') == 'md_bo_SZtPY6F5r6EpP23t') { ?>
            <div class = "form-group has-feedback">
                <label>Direção da Rede:</label>
                <select name = "direcao" class = "form-control">
                    <option {{($dados['ativo'] == 'esquerda') ? 'selected' : ''}} value = "esquerda">
                        Esquerda
                    </option>
                    <option { {($dados['ativo'] == 'direita') ? 'selected' : ''} } value = "direita">
                        Direita
                    </option>
                </select>
            </div>
            <div class = "form-group">
                <label>Pontos na direita:</label>
                <input type = "text" class = "form-control" name = "binario_direita" placeholder = "" value = "{{$dados['binario_direita']}}" />
            </div>
            <div class = "form-group">
                <label>Pontos na esquerda:</label>
                <input type = "text" class = "form-control" name = "binario_esquerda" placeholder = "" value = "{{$dados['binario_esquerda']}}" />
            </div>
            <?php
        }
        ?>
        <div class="form-group has-feedback">
            <?php
            $pacotes = App\Pacote::all();
            $meuPacote = App\Pacote::where('id', $dados['pacote'])->first();
            ?>
            <label>Pacote:</label>

            <select name="pacote" class="form-control">
                <option value = "{{$meuPacote->id}}" selected="">
                    {{$meuPacote->nome}}-R${{$meuPacote->valor}}
                </option>
                @foreach($pacotes as $pacote)
                <option value="{{$pacote->id}}">
                    {{$pacote->nome}}-R${{$pacote->valor}}
                </option>
                @endforeach
            </select>
            <p>*Não gera bonificação</p>

        </div>
        <div class="form-group">
            <input type="hidden" class="form-control"   name="id" value="{{$dados['id']}}" />

            <label>Nova Senha:</label>
            <input type="password" class="form-control"   name="password" value="" />
            <p>*Deixe esse campo vazio para mander a senha</p>
        </div>
        <div class="form-group">
            <label>Confirme a senha:</label>
            <input type="password" class="form-control"   name="password_confirmation" value="" />
        </div>

        <div class="form-group">
            <label>Cpf</label>
            <input type="text" class="form-control" required  name="cpf" value="{{$dados['cpf']}}" />
        </div>

        <div class="form-group">
            <label>Saldo</label>
            <input type="text" class="form-control" required placeholder="" value="{{$dados['saldo']}}" name="saldo" id='saldo'/>
        </div>
        <div class="form-group">
            <label>Saldo B</label>
            <input type="text" class="form-control" required placeholder="" value="{{$dados['carteira_b']}}" name="carteira_b" id='carteira_b'/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
</form>
<script>
    $('#valor').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#carteira_b').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
$('#saldo').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#valorIndicacao').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
</script>

