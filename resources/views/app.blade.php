<!DOCTYPE html>
<html lang="en">
    @include('layouts.partials.htmlheader')
    <body class="skin-black sidebar-mini">
        <div class="wrapper">
            @include('layouts.partials.mainheader')
            @include('layouts.partials.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @inject('renovacao', 'App\Http\Controllers\Admin\VoucherController')
                <?php
                if ($renovacao->renova(Auth::user()->id)) {
                    echo "<script>alert('A sua conta foi renovada automaticamente.');</script>";
                }
                ?>
                @include('layouts.partials.contentheader')
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                    @yield('main-content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
            @include('layouts.partials.controlsidebar')
            @include('layouts.partials.footer')
        </div><!-- ./wrapper -->
        <div class="modal fade" id="modalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalMsgTitle"></h4>
                    </div>
                    <div class="modal-body" id='modalMsgBody'>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-backdrop="static" 
             data-keyboard="false" >
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-sm">
                        <h4 class="modal-title" id="LoadingMsgTitle"></h4>
                    </div>
                    <div class="modal-body" id='LoadingMsgBody'>
                        <div class="text-center">
                            <img src="{{ asset('sximo/images/carregando.gif')}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.partials.scripts')
    </body>
</html>
