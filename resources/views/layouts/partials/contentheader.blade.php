<!-- Content Header (Page header) -->

<div class="row">
    <div class="col-md-12">
        @if(!Auth::user()->ativado())
        <div class="alert alert-warning alert-dismissible flat no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Atenção!
                <small class="text-bold text-black"> Sua conta encontra-se inativa, insira seu voucher de ativação abaixo ou efetue o pagamento para poder utilizar o sistema!</small>
            </h4>
            <form action="{{url('painel/ativar-conta')}}" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="input-group">
                    <input type="text" name="ativarVoucher" class="form-control" placeholder="Insira seu voucher de ativação">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Ativar Conta!</button>
                    </span>
                </div>
            </form>
        </div>
        @endif
        @if(Auth::user()->ativado() && !Auth::user()->isAdmin())
        <?php $retorno = Auth::user()->infoConsumo(); ?>
        @if(isset($retorno['msg']))
        <div class="alert alert-{{$retorno['tipo']}} alert-dismissible flat no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Atenção</strong><br>
            <ul>
                {{$retorno['msg']}}
            </ul>
        </div>
        @endif
        @endif
        @if(session('success'))
        <div class="alert alert-success fade in alert-dismissible flat  no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
            {{session('success')}}
        </div>
        @endif

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger alert-dismissible flat no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Whoops!</strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>


<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Page Header here')
        <small>@yield('contentheader_description')</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
        @yield('breadcrumb', '<li class="active">Dashboard</li>')
    </ol>
</section>