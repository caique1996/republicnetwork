<!-- Main Header -->
<header class="main-header">
    <?php
    global $request;
    $uriPainel = $request->segment(1);
    if (Auth::user()->isAdmin() and Auth::user()->ativo == 1 and $uriPainel != 'admin') {
        header('Location: ' . url('admin/home'));
        exit;
    }
    ?>

    <a href="{{ url('/'.$uriPainel.'/home') }}" class="logo" style="background: #FFFFFF;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"> <img src="<?= App\config::getConf()['logo_1'] ?>" /></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="<?= App\config::getConf()['logo_1'] ?>" /></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                <li><a href="{{ url('/'.$uriPainel.'/login') }}">Login</a></li>
                @else
                @if (! Auth::guest() && !Auth::user()->ativo)
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{Auth::user()->photo}}" width="64" class="user-image" alt="User Image" />
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{Auth::user()->getAvatar()}}" class="img-circle" alt="User Image"/>
                            <p>
                                {{ Auth::user()->name }}
                                <small>Membro desde {{Auth::user()->memberSince()}}</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('/'.$uriPainel.'/meus-dados') }}" class="btn btn-default btn-flat">Meus Dados</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/'.$uriPainel.'/logout') }}" class="btn btn-default btn-flat">Encerrar sessão</a>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->ativo)
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{Auth::user()->photo}}" width="64" class="user-image" alt="User Image"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img onclick="" class="img-circle" src="{{Auth::user()->photo}}">
                            <p>
                                {{ Auth::user()->name }}
                                <small>Membro {{Auth::user()->memberSince()}}</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('/'.$uriPainel.'/meus-dados') }}" class="btn btn-default btn-flat">Meus Dados</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/'.$uriPainel.'/logout') }}" class="btn btn-default btn-flat">Encerrar sessão</a>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif

                @endif

                <!-- Control Sidebar Toggle Button -->
                <?php if (!Auth::guest() && Auth::user()->isAdmin() && $uriPainel == 'admin') { ?>
                    <li>
                        <a href="{{ url('/'.$uriPainel.'/config') }}" ><i class="fa fa-gears"></i></a>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>
