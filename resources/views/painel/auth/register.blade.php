<?php ?>
@extends('layouts.auth')

@section('htmlheader_title')
<?= Lang::trans('site.register') ?>
@endsection

@section('content')
<style>
    @media (min-width:320px) { 

    }
    @media (min-width:480px) {

    }
    @media (min-width:600px) { 


    }
    @media (min-width:801px) { 
        .register-box{ width: 650px !important;}

    }
    @media (min-width:1200px) { 
        .register-box{ width: 650px !important;}
    }
</style>
<body class="hold-transition register-page" style="background-image:url('<?= \App\config::getConf()['logo_3'] ?>');background-size: cover;">
    <div class="register-box" >
        <div class="register-logo">
            <a href="{{ url('/') }}">
                <img src="<?= \App\config::getConf()['logo_1'] ?>">
            </a>
        </div>

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <?php if (@$_GET['huebr'] == 666) { ?>
            <div class="register-box-body">
                <?php if (isset($indicador->name)) { ?>
                    <p class="login-box-msg">Você foi indicado por: <b>{{$indicador->name. ' - '. $indicador->email}}</b></p>
                    <form action="{{ url('/cadastro/') }}" method="post" id="formcadastro">
                    <?php } ?>
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="col-sm-6">

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" id="usuario" required placeholder="ID DESEJADO " maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="teste<?= rand(1, 99999) ?>{{old('username')}}"/>

                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Id do patrocinador" value="<?= @$indicador->username ?>" maxlength="20" name="indicador" pattern="[a-zA-Z0-9]+" value="inovar{{old('indicador')}}"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Nome Completo" id="name" name="name" value="teste_<?= rand(1, 99999) ?> teste_<?= rand(1, 99999) ?>{{old('name')}}"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input name="cpf" type="text" required placeholder="Seu CPF" class="form-control" id="cpf"  value="<?= rand(1, 99999) ?>{{old('cpf')}}">
                        </div>

                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" required placeholder="Email" name="email" value="teste<?= rand(1, 99999) ?>@gmail.com{{old('email')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Senha" name="password" value="123456"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Repita a senha" name="password_confirmation" value="123456"/>
                        </div>

                        <div class="form-group has-feedback">
                            <select name="sexo" class="form-control">
                                <option value="Masculino" selected="">
                                    Masculino
                                </option>
                                <option value="Feminino">
                                    Feminino
                                </option>

                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value="27-07-1996" required=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control telefone" placeholder="Telefone" name="telefone" value="(75)98149-5203{{old('telefone')}}"/>
                        </div>
                    </div>
                    <div class="col-sm-6">





                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cep" id="cep" placeholder="CEP" name="cep" value="44010-125{{old('cep')}}" onBlur="buscaCEP(this.value);"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control estado" id="estado" placeholder="Estado: Ex: SP" name="estado" value="BA{{old('estado')}}"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cidade" id="cidade" placeholder="Cidade" name="cidade" value="Feira de santana{{old('cidade')}}"/>
                        </div>
                        <!---<div class="form-group has-feedback" disabled="" title="Selecione uma cidade">

                            <select name="cidade"  class="cidades form-control" >

                            </select>
                        </div>-->
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" id="endereco" placeholder="Endereço" name="endereco" value="Rua huebr brbr{{old('endereco')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" id="complemento" placeholder="Complemento" name="complemento" value="{{old('complemento')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control bairro" placeholder="Bairro" id="bairro" name="bairro" value="CIS{{old('bairro')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <a target="_blank" href="{{ url('/Contrato-e-Termos-de-Uso.pdf') }}">Clique aqui para ler os termos de uso.</a>
                        </div>
                        <div class="form-group has-feedback" >
                            <select name="pacote" class=" pacote form-control">

                                @foreach($pacotes as $pacote)
                                @if ($pacote->status==1)
                                <option value="{{$pacote->id}}">
                                    {{$pacote->nome}}-R${{$pacote->valor}}
                                    - <?php
                                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                                        echo $pacote->total_cotas . ' cota(s)';
                                    }
                                    ?>
                                </option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <select name="" id="termos" class="form-control">
                                <option value="0">Não aceito os termos</option>
                                <option value="1">Aceito os temos de uso</option>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" disabled="" required id="btEnviar">Enviar</button>
                        </div><!-- /.col -->
                    </div>

                </form>
            </div><!-- /.form-box -->
        <?php } else { ?>

            <div class="register-box-body">
                <?php if (isset($indicador->name)) { ?>
                    <p class="login-box-msg">Você foi indicado por: <b>{{$indicador->name. ' - '. $indicador->email}}</b></p>
                    <form action="{{ url('/cadastro/') }}" method="post" id="formcadastro">
                    <?php } ?>
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="col-sm-6">

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" id="usuario" required placeholder="ID DESEJADO " maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="{{old('username')}}"/>

                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Id do patrocinador" value="<?= @$indicador->username ?>" maxlength="20" name="indicador" pattern="[a-zA-Z0-9]+" value="inovar{{old('indicador')}}"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Nome Completo" id="name" name="name" value="{{old('name')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input name="cpf" type="text" required placeholder="Seu CPF" class="form-control" id="cpf"   value="{{old('cpf')}}">
                        </div>

                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" required placeholder="Email" name="email" value="{{old('email')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Senha" name="password" value=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Repita a senha" name="password_confirmation" value=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <select name="sexo" class="form-control">
                                <option value="Masculino" selected="">
                                    Masculino
                                </option>
                                <option value="Feminino">
                                    Feminino
                                </option>

                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value="" required=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control telefone" placeholder="Telefone" name="telefone" value="{{old('telefone')}}"/>
                        </div>
                    </div>
                    <div class="col-sm-6">





                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cep" id="cep" placeholder="CEP" name="cep" value="{{old('cep')}}" onBlur="buscaCEP(this.value);"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control estado" id="estado" placeholder="Estado: Ex: SP" name="estado" value="{{old('estado')}}"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cidade" id="cidade" placeholder="Cidade" name="cidade" value="{{old('cidade')}}"/>
                        </div>
                        <!---<div class="form-group has-feedback" disabled="" title="Selecione uma cidade">

                            <select name="cidade"  class="cidades form-control" >

                            </select>
                        </div>-->
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" id="endereco" placeholder="Endereço" name="endereco" value="{{old('endereco')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" id="complemento" placeholder="Complemento" name="complemento" value="{{old('complemento')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control bairro" placeholder="Bairro" id="bairro" name="bairro" value="{{old('bairro')}}"/>
                        </div>
                        <div class="form-group has-feedback" >
                            <select name="pacote" class=" pacote form-control">

                                @foreach($pacotes as $pacote)
                                @if ($pacote->status==1)
                                <option value="{{$pacote->id}}">
                                    {{$pacote->nome}}-R${{$pacote->valor}}
                                    - <?php
                                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                                        echo $pacote->total_cotas . ' cota(s)';
                                    }
                                    ?>
                                </option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <a target="_blank" href="{{ url('/Contrato-e-Termos-de-Uso.pdf') }}">Clique aqui para ler os termos de uso.</a>
                        </div>

                        <div class="form-group has-feedback">
                            <select name="" id="termos" class="form-control">
                                <option value="0">Não aceito os termos</option>
                                <option value="1">Aceito os temos de uso</option>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" disabled="" required id="btEnviar">Enviar</button>
                        </div><!-- /.col -->
                    </div>

                </form>
            </div><!-- /.form-box -->
        <?php } ?>


    </div><!-- /.register-box -->

</div>
@foreach($pacotes as $pacote)
@if ($pacote->status==1)
<div class="modal fade" data-backdrop="static" 
     data-keyboard="false" id="pacote-{{$pacote->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalMsgTitle">{{$pacote->nome}} - R${{$pacote->valor}} - <?php
                    if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                        echo $pacote->total_cotas . ' cota(s)';
                    }
                    ?></h4>
            </div>
            <div class="modal-body" id='modalMsgBody'>
                <?= $pacote->descricao ?>
            </div>
            <div id="mensagem_anuncio"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>

@endif
@endforeach

@include('layouts.partials.scripts_auth')
<?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
    <script src ='https://www.google.com/recaptcha/api.js' ></script>

<?php } ?>
<div id="modalLoginSel" class="modal fade" tabindex="-1" role="dialog"  data-backdrop="static" 
     data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Insira o login do seu indicador</h4>
            </div>
            <!-- -->
            @if (isset($errors) && count($errors) > 0  )
            <div class="alert alert-danger">
                <strong>Whoops!</strong><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form onsubmit="url = '<?php echo url('cadastro'); ?>' + '/' + $('#loginPtrocinador').val();
                                location.href = url;
                                return false;">
                <div class="modal-body">
                    <input type="text" id="loginPtrocinador" class="form-control" placeholder="login do seu indicador" required="" pattern="[a-zA-Z0-9]+" min="4">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok!</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@include('layouts.partials.scripts_auth')

<script>
    $(".pacote").change(function () {
        id = $(this).val();
        div_id = '#pacote-' + id;
        $(div_id).modal();
    });
    // Função para habilitar botão enviar ao clicar no checkbox aceito os termos
    $("#termos").click(function () {
        if ($(this).val() == 1) {
            $("#btEnviar").prop('disabled', false);
        } else {
            $("#btEnviar").prop('disabled', true);
        }
    });

    // Função de buscar usuários já cadastrados
    var typingTimer; //timer identifier
    var doneTypingInterval = 1000; //time in ms, 5 second for example

    //on keyup, start the countdown
    $('#usuario').keyup(function () {
        clearTimeout(typingTimer);
        if ($('#usuario').val) {
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        }
    });

    //user is "finished typing," do something
    function doneTyping() {
        $.ajax({
            'url': "<?= url('cadastro/verificarUsuario') ?>",
            "type": "POST",
            "data": "usuario=" + $('#usuario').val(),
            "datatype": 'html',
            'success': function (dados) {
                if (dados) {
                    $("#msgUsuario").remove();
                    $("#usuario").after('<label id="msgUsuario" ><p style="color:red">Usuário já existe.</a>Que tal você utilizar ' + $('#usuario').val() + Math.floor(Math.random() * 500) + '?</label>');
                } else {
                    $("#msgUsuario").remove();
                }
            }});
    }

    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('.data').inputmask("dd-mm-yyyy");
        //$('.cpf').inputmask("999.999.999-99");
        $('.cep').inputmask("99999-999");

        $("[data-mask]").inputmask();
    });
</script>
.<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72220910-1', 'auto');
    ga('send', 'pageview');
    function mascaraTelefone(campo) {

        function trata(valor, isOnBlur) {

            valor = valor.replace(/\D/g, "");
            valor = valor.replace(/^(\d{2})(\d)/g, "($1)$2");

            if (isOnBlur) {

                valor = valor.replace(/(\d)(\d{4})$/, "$1-$2");
            } else {

                valor = valor.replace(/(\d)(\d{3})$/, "$1-$2");
            }
            return valor;
        }

        campo.onkeypress = function (evt) {

            var code = (window.event) ? window.event.keyCode : evt.which;
            var valor = this.value

            if (code > 57 || (code < 48 && code != 8)) {
                return false;
            } else {
                this.value = trata(valor, false);
            }
        }

        campo.onblur = function () {

            var valor = this.value;
            if (valor.length < 13) {
                this.value = ""
            } else {
                this.value = trata(this.value, true);
            }
        }

        campo.maxLength = 14;
    }
    mascaraTelefone(formcadastro.telefone);

    function Apenas_Numeros(caracter)
    {
        var nTecla = 0;
        if (document.all) {
            nTecla = caracter.keyCode;
        } else {
            nTecla = caracter.which;
        }
        if ((nTecla > 47 && nTecla < 58)
                || nTecla == 8 || nTecla == 127
                || nTecla == 0 || nTecla == 9  // 0 == Tab
                || nTecla == 13) { // 13 == Enter
            return true;
        } else {
            return false;
        }
    }

    function maskCPF(CPF) {
        var evt = window.event;
        kcode = evt.keyCode;
        if (kcode == 8)
            return;
        if (CPF.value.length == 3) {
            CPF.value = CPF.value + '.';
        }
        if (CPF.value.length == 7) {
            CPF.value = CPF.value + '.';
        }
        if (CPF.value.length == 11) {
            CPF.value = CPF.value + '-';
        }
    }


</script>
<script src="<?= env('CFURL') ?>/dist/js/jcombo.js"></script>
<script src="<?= env('CFURL') ?>/plugins/input-mask/jquery.maskedinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/pt-BR.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<!-- CEP -->
<script type="text/javascript" >

    function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('endereco').value = ("");
        document.getElementById('bairro').value = ("");
        document.getElementById('cidade').value = ("");
        document.getElementById('estado').value = ("");
    }


</script>

<script type="text/javascript">
             $("#cpf").mask("999.999.999-99");


    $("#cpf_cnpj").change(function () {
        if ($(this).val() == 1) {
            $("#cpf").mask("99.999.999/9999-9");
            $("#name").attr('placeholder', 'Razão social');
            $("#cpf").attr('placeholder', 'Seu CNPJ');
        } else {
            $("#cpf").mask("999.999.999-99");
            $("#cpf").attr('placeholder', 'Seu CPF');


        }
    });
    $(document).ready(function () {
        //$(".estados").select2();
        /// $(".pacote").select2();


    });
    $(".estados").change(function () {
        if ($("#descCep").attr('data-cep') == 0) {
            $(".cidades").removeAttr('tabindex');
            $(".cidades").removeAttr('aria-hidden');
            // $(".cidades").select2();
            //$(".cidades").select2("destroy");
            $('.cidades')
                    .find('option')
                    .remove()
                    .end();
            valor = $(".estados").val();
            estadoSelecionado = valor;
            url = '<?= env('SITE_URL') ?>localizacao/cidade?id=' + estadoSelecionado + "&tp=2"
            $.get(url, function (data) {
                $(".cidades").html(data);
            });
            // $(".cidades").select2();
        }


    });
    function buscaCEP(cep) {

        limpa_formulário_cep();

        cep.replace("-", "");
        $("#descCep").html('Procurando....');
        url = 'http://api.postmon.com.br/v1/cep/' + cep;

        $.getJSON(url, function (data) {
            $("#descCep").html('');
            $("#cidade").val(data.cidade);
            $("#estado").val(data.estado);
            $("#bairro").val(data.bairro);
            $("#endereco").val(data.logradouro + ' - nº ');
        });
    }
<?php if (!isset($indicador->name)) { ?>
        $("#modalLoginSel").modal();

<?php } ?>


</script>

</body>

@endsection
