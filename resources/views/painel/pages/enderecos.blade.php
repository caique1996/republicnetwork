@extends('layouts.app')

@section('htmlheader_title')
Selecionar endereço
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Loja Virtual > Meu carrinho > Endereços
@endsection

@section('contentheader_description')

@endsection
<?php

function validarCep($cep) {
    return preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $cep);
}
?>
@section('main-content')
<style>
    .freteSedex,.fretePac{
        display:none;

    }
</style>
<div class="container-fluid">

    <div class="box">
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">

                    <div id="mensagemAdicionarVouchers">

                    </div>
                    <form action="index.html" method="get">
                        <div class="col-md-5">
                            <h4>Selecione o serviço para envio do pedido *</h4>

                            <div class="row">
                                <div class="box">
                                    <div class="box-body">
                                        <table class="table table-bordered">

                                            <tr>
                                                <td>
                                                    <p>PAC</p>
                                                </td>
                                                <td>
                                                    <input type="radio" name="codigo" value="41106" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>SEDEX</p>
                                                </td>
                                                <td>
                                                    <input type="radio" name="codigo" value="40010" required>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <h4>Selecione o endereço para realizar o pedido *</h4>
                            @foreach($enderecos as $endereco)
                            @if(validarCep($endereco['cep']))
                            <div class="row">
                                <div class="box box-info">
                                    <div class="box-body">

                                        <?php
                                        /*   $altura = 2;
                                          $largura = '11';
                                          $comprimento = '16';
                                          # Código dos Serviços dos Correios
                                          # 41106 PAC sem contrato
                                          # 40010 SEDEX sem contrato
                                          # 40045 SEDEX a Cobrar, sem contrato
                                          # 40215 SEDEX 10, sem contrato

                                          $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx" .
                                          "?nCdEmpresa=&sDsSenha=&sCepOrigem=" . \App\User::where('id', 1)->first()->cep .
                                          "&sCepDestino=" . $endereco['cep'] . "&nVlPeso=" . $pesoTotal .
                                          "&nCdFormato=1&nVlComprimento=" . $comprimento .
                                          "&nVlAltura=" . $altura . "&nVlLargura=" . $largura .
                                          "&sCdMaoPropria=n&nVlValorDeclarado=" . 17 .
                                          "&sCdAvisoRecebimento=n&nCdServico=" . 41106 .
                                          "&nVlDiametro=0&StrRetorno=xml";


                                          $xml = simplexml_load_file($correios);
                                          $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx" .
                                          "?nCdEmpresa=&sDsSenha=&sCepOrigem=" . \App\User::where('id', 1)->first()->cep .
                                          "&sCepDestino=" . $endereco['cep'] . "&nVlPeso=" . $pesoTotal .
                                          "&nCdFormato=1&nVlComprimento=" . $comprimento .
                                          "&nVlAltura=" . $altura . "&nVlLargura=" . $largura .
                                          "&sCdMaoPropria=n&nVlValorDeclarado=" . 17 .
                                          "&sCdAvisoRecebimento=n&nCdServico=" . 40010 .
                                          "&nVlDiametro=0&StrRetorno=xml";


                                          $xml2 = simplexml_load_file($correios);
                                          if ($xml->cServico->Erro == '0' and $xml2->cServico->Erro) {
                                          $entregaAproximada1 = date('d-m-Y', strtotime('+ ' . $xml->cServico->PrazoEntrega . ' days', strtotime(date('d-m-Y'))));
                                          $valorFrete1 = $xml->cServico->Valor;
                                          $valorFrete1 = (float) str_replace(',', '.', $valorFrete1);
                                          $total1 = $valorFrete1 + $subtotal;

                                          $entregaAproximada2 = date('d-m-Y', strtotime('+ ' . $xml2->cServico->PrazoEntrega . ' days', strtotime(date('d-m-Y'))));
                                          $valorFrete2 = $xml2->cServico->Valor;
                                          $valorFrete2 = (float) str_replace(',', '.', $valorFrete2);
                                          $total2 = $valorFrete2 + $subtotal; */
                                        ?>
                                        <p>Endereço para entrega</p>
                                        <p>{{$endereco['pais']}}</p>
                                        <p>{{$endereco['endereco']}}</p>
                                        <p>Total: R$ {{$subtotal}}</p>

                                        <p>CEP {{$endereco['cep']}} - {{$endereco['cidade']}}, {{$endereco['estado']}}</p>

                                        <a  class="codFrete" data-href="/painel/meus-pedidos/add/{{$endereco['id']}}/[cod]?pagamento=3"><button type="button" class="btn btn-info">Pagar com PagSeguro(boleto e cartão de crédito)</button></a>
                                        <div class="clearfix"></div>




                                    </div>
                                </div>
                            </div>

                            @endif
                            @endforeach

                            @if(sizeof($enderecos) == 0)
                            <div class="callout callout-info">
                                <h4>Não existem endereços cadastrados!</h4>
                                <p>Para prosseguir, cadastre um endereço que poderá receber o produto.</p>
                            </div>
                            @endif

                        </div>

                    </form>

                    <div class="col-md-offset-1 col-md-6">
                        <form method="post" action="/painel/meu-carrinho/endereco/add">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>

                                <!-- Form Name -->
                                <legend>Adicionar novo endereço</legend>
                                @if(!empty($error))
                                <div class="callout callout-danger">
                                    <h4>{{$error}}</h4>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="control-label" for="Country">País</label>
                                    <select id="Country" name="pais" class="form-control">
                                        <option value="Brasil">Brasil</option>
                                    </select>
                                </div>


                                <div class="form-group has-feedback">
                                    <select name="estado" id="estado" class="form-control" required="">
                                        <?php
                                        $estados = DB::table('estado')->get();
                                        foreach ($estados as $value) {
                                            echo '<option value="' . $value->uf . '">' . $value->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="city">Cidade</label>
                                    <input id="city" name="cidade" required="" type="text" placeholder="Sua cidade" class="form-control input-md" required="">

                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="city">Bairro</label>
                                    <input id="city" name="bairro" required="" type="text" placeholder="Seu Bairro" class="form-control input-md" required="">
                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="cep">CEP</label>
                                    <input id="cep" name="cep" type="text" placeholder="Insira seu cep" class="form-control input-md" required="">
                                    <span class="help-block"><a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sabe o seu CEP? Clique aqui.</a></span>

                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label" for="address1">Endereço</label>
                                    <input id="address1" name="endereco" type="text" placeholder="" class="form-control input-md">
                                    <span class="help-block">Endereço da sua casa, compania ou outro...</span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="address1">Número</label>
                                    <input id="address1" name="numero" type="text" placeholder="" class="form-control input-md">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="address1">Complemento</label>
                                    <input id="address1" name="complemento" type="text" placeholder="" class="form-control input-md">
                                </div>

                            </fieldset>
                            <button type="submit" class="btn btn-info pull-right">Adicionar novo</button>
                        </form>


                    </div>

                </div>
            </div>
        </div>


    </div>

</div>

@endsection

@section('page_scripts')
<script>
    $("input[name='codigo']").click(function () {
        if ($(this).val() == '41106') {
            $(".fretePac").show();
            $(".freteSedex").hide();
            alert("Pac selecionado");

        }
        if ($(this).val() == '40010') {
            $(".fretePac").hide();
            $(".freteSedex").show();
            alert("Sedex selecionado");


        }
    });
    $(".codFrete").click(function () {
        radio = $("input[name='codigo']:checked").val();
        if (radio == null) {
            alert('Selecione um meio de envio.');
            return false;

        } else {
            link = $(this).attr('data-href');
            link = link.replace('[cod]', radio);
            $.getJSON(link, function (data) {
                if (data.url == 'error') {
                    alert(data.message[0]);
                    return false;
                } else {
                    PagSeguroLightbox({
                        code: data.code[0]
                    }, {
                        success: function () {
                            location.href = "<?= url('painel/faturas?payment_succes') ?>";

                        },
                        abort: function () {
                            location.reload();


                        }
                    });
                }
                return false;


            });
        }

    });
</script>
@if(env('PAGSEGURO_SANDBOX'))
<script type="text/javascript"
src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
@else
<script type="text/javascript"
src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
@endif
@endsection
