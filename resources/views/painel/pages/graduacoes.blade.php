@extends('layouts.app')

@section('htmlheader_title')
Graduações
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Graduações
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Graduações</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Requisitos</th>
                            <th>Premiação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('graduacoes', 'App\graduacoes')
                        @inject('usr', 'App\User')
                        @foreach($graduacoes->where('status',1)->get() as $graduacao)
                        <tr>
                            <td ><img src="{{$graduacao['icone']}}" width="60"></td>
                            <td>{{$graduacao['name']}}</td>
                            <?php
                            $requisitos['pontuacao'] = Auth::user()->total_bin_dir - $graduacao['pontuacao'];

                            if ($requisitos['pontuacao'] >= 0) {
                                $necesario = 0;
                                $bin_ok = '<i class="fa fa-check bg-green-active"></i>';
                            } else {
                                $necesario = $graduacao['pontuacao'] - Auth::user()->total_bin_dir;
                                $bin_ok = '<i class="fa fa-close bg-red"></i>';
                            }
                            $qntd_ok = '';
                            ?>

                            <td>Você precisa de mais {{$necesario}} pontos <?= @$bin_ok ?><br>
                                <?php if ($necesario == 0) { ?>
                                    Caso você ainda não tenha solicitado,você deve solicitar a premiação dessa graduação.<i class="fa fa-check bg-green-active"></i>
                                <?php } ?>

                            </td>
                            <td><a class="premiacao" premiacao-id="{{$graduacao['id']}}">Ver premiação</a>
                                

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>
<?php
$graduacoes=  \App\graduacoes::where('status',1)->get();
?>
@foreach($graduacoes as $graduacao)
@if ($graduacao->status==1)
<div class="modal fade" data-backdrop="static" 
     data-keyboard="false" id="premiacao-{{$graduacao->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalMsgTitle">{{$graduacao->name}} - {{$graduacao->pontuacao}} pontos </h4>
            </div>
            <div class="modal-body" id='modalMsgBody'>
                <?=$graduacao->premios?>
            </div>
            <div id="mensagem_anuncio"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>

@endif
@endforeach
@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
     $(".premiacao").click(function () {
        id = $(this).attr('premiacao-id');
        div_id = '#premiacao-' + id;
        $(div_id).modal();
    });
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[0, "desc"]],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
@endsection
