@extends('layouts.app')

@section('htmlheader_title')
Dashboard
@endsection

@section('contentheader_title')

@endsection

@section('breadcrumb')
@endsection

@section('contentheader_description')

@endsection

@section('main-content')

<!-- Main row -->
<div class="row">
    <section class="col-lg-12">
        <div class="callout callout-info">
            <h4>Parabéns, o seu cadastro foi realizado com sucesso!</h4>
    </section>


    <section class="col-lg-6">

        <div class="panel panel-info text-center" style="border: none;">
            <div class="panel-heading" style="background-color: #4b4b4b; color: #fff; border-color: none;">
                Efetuar Pagamento via PagSeguro
            </div>
            <br>
            <img src="{{url('img/logo-pagseguro.png')}}" style="width: 70%; margin-top: -50px;">

            <a data-metodo="5" target="_blank" class="btn btn-lg btn-primary gerarBoleto">Clique aqui para pagar</a>
            <div class="clearfix"></div>
            <br>
            <br>
        </div>

    </section><!-- /.Left col -->

    <section class="col-lg-6">

        <div class="panel panel-info text-center" style="border: none;">
            <div class="panel-heading" style="background-color: #4b4b4b; color: #fff; border-color: none;">
                Via transferência bancária
            </div>
            <div class="panel-body">
                <a id="transferencia"  target="_blank" class="btn btn-lg btn-primary">Pagar Via transferência bancária</a>

            </div>
        </div>
</div>


</div><!-- /.row (main row) -->
<!-- REQUIRED JS SCRIPTS -->
<div id='transferenciaBan' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dados das contas</h4>

            </div>
            <div class="modal-body">
                <?= $config['deposito_contas'] ?>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    @endsection

    @section('page_scripts')
    <!-- iCheck 1.0.1 -->
    <script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/form/jquery.form.min.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/iCheck/icheck.min.js') }}"></script>
    @if(env('PAGSEGURO_SANDBOX'))
    <script type="text/javascript"
    src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
    @else
    <script type="text/javascript"
    src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
    @endif
    <script type='text/javascript'>

function fBlockUi() {
    $.blockUI({
        message: "<h4>Por favor aguarde...</h4>",
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .5,
            color: '#fff'
        }
    });
}

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
});
$("#transferencia").click(function () {
    $("#transferenciaBan").modal();
});
$(".gerarBoleto").click(function () {
    elemento = $(this);
    elemento.html('Por favor aguarde...');
    link = "?novopagamento=1&metodo=" + elemento.attr('data-metodo');
    $.getJSON(link, function (data) {
        if (data.url == 'error') {
            alert(data.message[0]);
            return false;
        } else {
            elemento.removeClass('btn-primary');
            elemento.addClass('btn-success')
            elemento.html('Carregando...');
            PagSeguroLightbox({
                code: data.code[0]
            }, {
                success: function () {
                    location.href = "<?= url('painel/faturas?payment_succes') ?>";

                },
                abort: function () {
                    location.reload();


                }
            });
        }

    });
});
/**/
    </script>
    <div id='voucher' class="modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <p>


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Selecionar quantidade de Vouchers</h4>

                </div>
                <div class="modal-body">

                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="1">01</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="2">02</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="3">03</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="4">04</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="0">Nenhum</label>
                    </div>
                    <p>Valor total a pagar: R$<span id="valorPagar">200</span></p>


                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="gerarBoleto" class="btn btn-primary">Gerar Boleto</button>
                        </p>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        @endsection

        @section('page_scripts')
        <!-- iCheck 1.0.1 -->
        <script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>
        <script src="{{ env('CFURL').('/plugins/form/jquery.form.min.js') }}"></script>
        <script src="{{ env('CFURL').('/plugins/iCheck/icheck.min.js') }}"></script>
        <script type='text/javascript'>

function fBlockUi() {
    $.blockUI({
        message: "<h4>Por favor aguarde...</h4>",
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .5,
            color: '#fff'
        }
    });
}

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
});

$("#gerarBoleto").click(function () {
    $('#gerarBoleto').html('Por favor aguarde...');
    $.ajax({
        'url': "?novopagamento=1",
        dataType: 'html',
        'success': function (txt) {
            if (txt == '') {
                alert('Houve uma falha ao gerar o boleto');
            } else {
                $('#gerarBoleto').removeClass('btn-primary');
                $('#gerarBoleto').addClass('btn-success')
                $('#gerarBoleto').html('Redirecionando...');
                setTimeout(function () {
                    location.href = txt;
                }, 2000);
            }

        }
    });

});
/**/
        </script>

        @endsection