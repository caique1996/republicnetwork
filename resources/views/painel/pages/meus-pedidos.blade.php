@extends('layouts.app')

@section('htmlheader_title')
Meus pedidos
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Meus pedidos > Lista de pedidos
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box">
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="ord-carrinho" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>ID</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Mensagem</th>
                            <th>Detalhes</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pedidos as $pedido)
                        <tr>
                            <td style="
                                background-image: url({!! url(''.$pedido['img']) !!});
                                background-position: center;
                                background-size: cover;">
                            </td>
                            <td>{{$pedido['id']}}</td>
                            <td>{{Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')}}</td>
                            <td>{{number_format($pedido['preco'],2)}}</td>
                            <td>{{$pedido['status']}}</td>
                            <td><?=$pedido['info']?></td>

                            <td><a href="{!! url('painel/meus-pedidos/pedido/'.$pedido['id']) !!}" class="btn btn-info">Detalhes</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {

    $('#ord-carrinho').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        "oLanguage": {"sZeroRecords": "Você ainda não realizou nenhum pedido",
            "sEmptyTable": "Você ainda não realizou nenhum pedido"},
        "order": [[1, "desc"]]
    });
});
</script>
@endsection
