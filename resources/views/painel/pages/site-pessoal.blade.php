<?php
$config=  App\config::getConf();
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= $usr['username'] ?> - <?=$config['site_name']?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">


        <!--[if lt IE 9]>
            <script src="../pagina-pessoal/js/html5shiv.js"></script>
            <![endif]-->


        <!-- CSS Files
        ================================================== -->
        <link rel="stylesheet" href="../pagina-pessoal/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/jpreloader.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/animate.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/flexslider.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/prettyPhoto.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/owl.carousel.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/owl.theme.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/style.css" type="text/css">
        <link rel="icon" type="image/png" href="../pagina-pessoal/images/favicon.png" />


        <!-- custom style css -->
        <link rel="stylesheet" href="../pagina-pessoal/css/custom-style.css" type="text/css">

        <!-- color scheme -->
        <link rel="stylesheet" href="../pagina-pessoal/css/color.css" type="text/css">

        <!-- revolution slider -->
        <link rel="stylesheet" href="../pagina-pessoal/rs-plugin/css/settings.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/css/rev-settings.css" type="text/css">

        <!-- load fonts -->
        <link rel="stylesheet" href="../pagina-pessoal/fonts/font-awesome/css/font-awesome.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
        <link rel="stylesheet" href="../pagina-pessoal/fonts/et-line-font/style.css" type="text/css">
    </head>

    <body id="homepage">


        <!-- This section is for Splash Screen -->
        <div id="jSplash">
            <section class="selected">
                Carregando...
            </section>
        </div>
        <!-- End of Splash Screen -->


        <div id="wrapper">
            <div class="page-overlay">
            </div>


            <!-- header begin -->
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- logo begin -->
                            <h1 id="logo">
                                <a href="">
                                    <img class="logo-1" src="<?=$config['logo_1']?>" alt="<?=$config['site_name']?>">
                                    <img class="logo-2" src="<?=$config['logo_1']?>" alt="<?=$config['site_name']?>">
                                </a>
                            </h1>
                            <!-- logo close -->

                            <!-- small button begin -->
                            <span id="menu-btn"></span>
                            <!-- small button close -->

                            <!-- mainmenu begin -->
                            <nav>
                                <ul id="mainmenu">
                                    <li><a class="active" href="#wrapper">INÍCIO</a></li>
                                    <li><a href="#section-about">SOBRE NÓS</a></li>
                                    <li><a href="#section-services">PRODUTO</a></li>
                                    <li><a href="<?= url('painel/login') ?>" target="_blank">BACKOFFICE</a></li>
                                    <li><a href="#section-contact">CONTATO</a></li>
                                </ul>
                            </nav>

                        </div>
                        <!-- mainmenu close -->

                    </div>
                </div>
            </header>
            <!-- header close -->


            <!-- content begin -->
            <div id="content" class="no-bottom no-top">


                <!-- section begin -->
                <section class="full-height dark no-padding dark" data-speed="5" data-type="background">
                    <div class="de-video-container">
                        <div class="de-video-content">
                            <div class="text-center" style="margin-top:70px">
                                VOCÊ ESTÁ PREPARADO PARA O QUE VEM POR AI?
                                <div class="text-slider big-text">
                                    <div class="text-item"><span class="id-color">+</span> <b>Mudança</b></div>
                                    <div class="text-item"><span class="id-color">+</span> <b>Resultado</b></div>
                                    <div class="text-item"><span class="id-color">+</span> <b>Impacto</b></div>
                                </div>
                                <div class="spacer-single"></div>
                            </div>
                        </div>

                        <div class="de-video-overlay"></div>

                        <!-- load your video here -->
                        <video autoplay="" poster="../pagina-pessoal/images/background/bg-5.jpg">
                            <source src="../pagina-pessoal/video/video.mp4" type="video/mp4" />
                        </video>


                    </div>

                </section>
                <!-- section close -->


                <!-- section begin -->
                <section id="section-about">
                    <div class="container">
                        <div class="row">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 text-center">
                                    <h2 class="animated" data-animation="fadeInUp"><b>NOSSOS PILARES</b></h2>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                    </h1>
                                    <p class="lead animated" data-animation="fadeInUp">
                                        Quando se tem um porquê, tudo fica mais fácil!<br> Juntos, iremos criar a mudança, impactar vidas e trazer resultado.
                                    </p>
                                    <div class="spacer-single"></div>
                                </div>
                            </div>
                            <!-- featured box begin -->
                            <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="0">
                                <div class="inner">
                                    <div class="front">
                                        <i class="icon-genius"></i>
                                        <h3><?= $landing['pilar_1_titulo'] ?></h3>
                                    </div>
                                    <div class="info">
                                        <?= $landing['pilar_1_conteudo'] ?>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <!-- featured box close -->
                            <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="400">
                                <div class="inner">
                                    <div class="front">
                                        <i class="icon-layers"></i>
                                        <h3><?= $landing['pilar_2_titulo'] ?></h3>
                                    </div>
                                    <div class="info">
                                        <?= $landing['pilar_2_conteudo'] ?>                                	<br>
                                    </div>
                                </div>
                            </div>
                            <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="200">
                                <div class="inner">
                                    <div class="front">
                                        <i class="icon-linegraph"></i>
                                        <h3><?= $landing['pilar_3_titulo'] ?> </h3>
                                    </div>
                                    <div class="info">
                                        <?= $landing['pilar_3_conteudo'] ?> 
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="600">
                                <div class="inner">
                                    <div class="front">
                                        <i class="icon-heart"></i>
                                        <h3><?= $landing['pilar_4_titulo'] ?> </h3>
                                    </div>
                                    <div class="info">
                                        <?= $landing['pilar_4_conteudo'] ?>
                                        <br>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>

                <!-- section begin -->
                <section id="section-about-us-2" class="side-bg no-padding">

                    <div class="container">
                        <div class="row">
                            <div class="inner-padding">
                                <div class="col-md-6" data-animation="fadeInRight" data-delay="200">
                                    <iframe width="560" height="315" src="<?= str_replace('watch?v=', 'embed/', $landing['youtube_video']); ?>" frameborder="0" allowfullscreen style="border:solid 15px rgba(255, 255, 255, 1); margin-left:-50px"></iframe>

                                </div>
                                <div class="col-md-6" data-animation="fadeInRight" data-delay="200">
                                     <h2><b>A OPORTUNIDADE</b></h2>

                                    <p class="intro">	A empresa <?=$config['site_name']?> foi fundada em Janeiro de 2012, sendo a primeira empresa especializada em Midias Sociais, Publicidade, e Marketing Estratégico.
                                        Nossa base operacional e de desenvolvimento, está localizada em um dos mais tradicionais edifícios comerciais de Singapura (7030 ANG Mo Kio Avenue 5, Northstar @ Amk.)
                                        Com a implementação de dois grandes mercados, iniciamos um sistema de comissionamento inteligente e sustentável, onde oferecemos a oportundiade de transformação financeira e social aos nossos associados.
                                        <br>
                                        Através de nossos produtos, podemos oferecer resultados reais, com segmentações precisas, custo baixo e ótimo custo x benefício aos nossos clientes e parceiros. 
                                        Somos dententores de uma base de dados com aproximadamente quatro milhões de clientes ativos, que estão localizados em mais de quarenta países ao redor do mundo.

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- section close -->
                <!-- section begin -->
                <section class="no-padding dark" data-speed="5" data-type="background" style="background-image:url(../pagina-pessoal/conferencia.jpg)">
                    <div class="de-video-container">
                        <div class="de-video-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 text-center">
                                        <h2 class="animated" data-animation="fadeInUp"><b>CONFERENCIA <span class="id-color">ONLINE</b></span>
                                            <span class="small-border animated" data-animation="fadeInUp"></span>
                                        </h2>
                                        <div class="animated" data-animation="fadeInUp">
                                            Oferecemos uma oportunidade única para você realizar uma mudança completa em sua vida. Clicando no botão abaixo você poderá assistir uma de nossas apresentações online e tirar suas dúvidas em tempo real.
                                            <div class="spacer-single"></div>
                                            <br>
                                            <a href="<?= $landing['conferencia_link'] ?>" class="btn btn-border" data-animation="fadeIn">Acessar conferência online!</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>
                <!-- section close -->

                <!-- section begin -->
                <section id="section-services" class="no-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h2 class="animated" data-animation="fadeInUp"><b>LINKS <span class="id-color">ÚTEIS</b></span>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                </h2>
                                <p class="lead animated" data-animation="fadeInUp">
                                    Abaixo você encontra fácilmente os links úteis!
                                </p>
                                <div class="spacer-single"></div>
                            </div>
                        </div>

                        <div class="row">

                            <!-- feature box begin -->
                            <div class="feature-box-small-icon col-md-6 animated" data-animation="fadeInUp" data-delay="600">
                                <div class="inner">
                                    <i class="icon-phone"></i>
                                    <div class="text">
                                        <h3>Slides de Apresentação</h3>
                                        <a href="#" target="_blank"><font color="black">Para acessar os slides de apresentação ADSPLY, clique aqui.</font></a>
                                    </div>
                                </div>
                            </div>
                            <!-- feature box close -->


                            <!-- feature box begin -->
                            <div class="feature-box-small-icon col-md-6 animated" data-animation="fadeInUp" data-delay="800">
                                <div class="inner">
                                    <i class="icon-layers"></i>
                                    <div class="text">
                                        <h3>Escritório Virtual</h3>

                                        <a href="<?= env('SITE_URL') ?>painel/login" target="_blank"><font color="black"> Para acessar a sua conta em nossa Plataforma Virtual, clique aqui.</a>
                                    </div>
                                </div>
                            </div>
                            <div class="spacer-single"></div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </section>
                <!-- section close -->
                <section id="view-all-projects" class="call-to-action bg-color dark text-center" data-stellar-background-ratio=".2" style="background-position: 0px 534.4px; background-color:#214e6e">
                    <a href="<?=env('SITE_URL')?>cadastro/<?= $usr['username'] ?>" class="btn btn-border btn-big">CADASTRE-SE AGORA MESMO!</a>
                </section>


                <!-- section begin -->
                <section id="section-contact" class="dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2 class="animated" data-animation="fadeInUp"><b>ENTRE EM CONTATO<span class="id-color"> COMIGO</b></span>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                </h2>
                                <p class="animated" data-animation="fadeIn">
                                    Estou pronto para receber suas dúvidas, sugestões e críticas :).
                                </p>
                                <div class="spacer-single"></div>
                            </div>

                            <div class="col-md-8 animated" data-animation="fadeInUp" data-delay="200" data-speed="5">

                                <form name="contactForm" id='contact_form' method="post" action='enviar_email/<?= $usr['username'] ?>'>
                                                                                    <?= csrf_field() ?>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id='name_error' class='error'>Por favor, preencha o campo nome.</div>
                                            <div>
                                                <input type='text' name='name' id='name' class="form-control" placeholder="Nome">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id='email_error' class='error'>Preencha um e-mail válido.</div>
                                            <div>
                                                <input type='text' name='email' id='email' class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id='message_error' class='error'>O campo de mensagem não pode ser vazio.</div>
                                            <div>
                                                <textarea name='message' id='message' class="form-control" placeholder="Mensagem"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                                        <div class='g-recaptcha'  data-sitekey='6LdPNRsTAAAAAAPrpcPLZmHFzAwKOv_kMpV0ivd2'></div>
                                                        <br>
                                                    </div>
                                        <div id='mail_success' class='success'>Sua mensagem foi enviada com sucesso. Obrigado!</div>
                                        <div id='mail_fail' class='error'>Ops, parece que ocorreu um erro ao enviar a sua mensagem.</div>
                                        <div class="col-md-12">
                                            <p id='submit'>
                                                <input type='submit' id='send_message2' value='Enviar mensagem' class="btn btn-border">
                                            </p>
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <div class="col-md-4">
                                <br>
                                <address>
                                            <span><i class="fa fa-map-marker fa-lg"></i><?= $usr['cidade'] ?>, <?= $usr['estado'] ?>,<?= $usr['pais'] ?> </span>
                                            <span><i class="fa fa-envelope-o fa-lg"></i><a href="mailto:<?= $landing['email'] ?>"><?= $landing['email'] ?></a></span>
                                        </address>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- section close -->

                <!-- footer begin -->
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <?=$config['site_name']?> - Página de Captura para associados - Quase todos direitos reservados
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- footer close -->
            </div>
        </div>


        <!-- Javascript Files
        ================================================== -->
        <script src="../pagina-pessoal/js/jquery.min.js"></script>
        <script src="../pagina-pessoal/js/jpreLoader.js"></script>
        <script src="../pagina-pessoal/js/bootstrap.min.js"></script>
        <script src="../pagina-pessoal/js/jquery.isotope.min.js"></script>
        <script src="../pagina-pessoal/js/jquery.prettyPhoto.js"></script>
        <script src="../pagina-pessoal/js/easing.js"></script>
        <script src="../pagina-pessoal/js/jquery.ui.totop.js"></script>
        <script src="../pagina-pessoal/js/jquery.flexslider-min.js"></script>
        <script src="../pagina-pessoal/js/jquery.scrollto.js"></script>
        <script src="../pagina-pessoal/js/owl.carousel.js"></script>
        <script src="../pagina-pessoal/js/jquery.countTo.js"></script>
        <script src="../pagina-pessoal/js/classie.js"></script>
        <script src="../pagina-pessoal/js/designesia.js"></script>
        <script src="../pagina-pessoal/js/validation.js"></script>

        <!-- SLIDER REVOLUTION SCRIPTS  -->
        <script type="text/javascript" src="../pagina-pessoal/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="../pagina-pessoal/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    </body>
</html>
