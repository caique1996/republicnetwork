<?php $__env->startSection('htmlheader_title'); ?>
Meus Dados
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Meus Dados
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>

<div class="row">

    <section class="col-lg-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Dados Pessoais</a></li>
                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Dados Bancários</a></li>

                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Senha de segurança</a></li>
                <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Senha de acesso</a></li>
            </ul>

            <form role="form" method="post" action="">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('POST')); ?>

                <div class="box-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">  <div class="form-group has-feedback">
                                <img onclick="" class="img-circle" src="<?php echo e(Auth::user()->photo); ?>" width="128" data-container="body" alt="User Avatar" data-html='true'  title="Mudar Foto"  data-toggle="popover" data-content="<form  method='post' action='mudar_foto' enctype='multipart/form-data'><div class='form-group'>
                                     <input type='file' id='photo' name='image' style='display:none'>
                                     <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>' />
                                     <a onclick='selecionarFoto();' class='btn btn-block btn-warning btn-sm'>Selecionar Foto</a>
                                     <button id='salvarFoto' style='display:none;' class='btn btn-block btn-success btn-sm'>Salvar</button>

                                     </div></form>">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" required placeholder="Nome Completo" disabled="" name="name" value="<?php echo e(old('name') ? old('name') : Auth::user()->name); ?>"/>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="email" class="form-control" readonly required placeholder="Email" name="email" value="<?php echo e(old('email') ? old('email') : Auth::user()->email); ?>"/>
                            </div>




                    <div class="form-group has-feedback">
                        <label>Sexo:</label>
                        <select name="sexo" class="form-control">
                            <option <?php echo e((old('sexo') == 'Masculino' OR Auth::user()->sexo == 'Masculino') ? 'selected' : ''); ?> value="Masculino">
                                Masculino
                        </option>
                        <option <?php echo e((old('sexo') == 'Feminino' OR Auth::user()->sexo == 'Feminino') ? 'selected' : ''); ?> value="Feminino">
                            Feminino
                    </option>
                </select>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control"  placeholder="Nome do segundo titular" name="segtitular_nm" value="<?php echo e(old('segtitular_nm') ? old('segtitular_nm') : Auth::user()->segtitular_nm); ?>"/>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control cpf"  placeholder="CPF do segundo titular" name="segtitula_cpf" value="<?php echo e(old('segtitula_cpf') ? old('segtitula_cpf') : Auth::user()->segtitula_cpf); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control cpf" required placeholder="CPF" disabled="" name="cpf" value="<?php echo e(old('cpf') ? old('cpf') : Auth::user()->cpf); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control data" placeholder="Data de Nascimento" disabled="" name="nascimento" value="<?php echo e(old('nascimento') ? old('nascimento') : Auth::user()->getNascimento()); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control telefone" data-inputmask="'mask': ['99-9999-9999[9]', '+99 99 9999-9999[9]']" data-mask placeholder="Telefone" name="telefone" value="<?php echo e(old('telefone') ? old('telefone') : Auth::user()->telefone); ?>"/>
            </div>


            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Endereço" name="endereco" value="<?php echo e(old('endereco') ? old('endereco') : Auth::user()->endereco); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Bairro" name="bairro" value="<?php echo e(old('bairro') ? old('bairro') : Auth::user()->bairro); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Cidade" name="cidade" value="<?php echo e(old('cidade') ? old('cidade') : Auth::user()->cidade); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Estado" name="estado" value="<?php echo e(old('estado') ? old('estado') : Auth::user()->estado); ?>"/>
            </div>

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Banco" name="banco" value="<?php echo e(old('banco') ? old('banco') : Auth::user()->banco); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Agência" name="agencia" value="<?php echo e(old('agencia') ? old('agencia') : Auth::user()->agencia); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Conta" name="conta" value="<?php echo e(old('conta') ? old('conta') : Auth::user()->conta); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Tipo de Conta" name="tipo_conta" value="<?php echo e(old('tipo_conta') ? old('tipo_conta') : Auth::user()->tipo_conta); ?>"/>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Operação" name="operacao" value="<?php echo e(old('operacao') ? old('operacao') : Auth::user()->operacao); ?>"/>
            </div>
        </div>
        <div class="tab-pane" id="tab_3">
            <div class="btn btn-danger" onclick="codigoSeg()">Redefinir o código de segurança e enviar via e-mail</div>
        </div>
        <div class="tab-pane" id="tab_4">
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Senha Atual" name="current_password"/>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Senha" name="password"/>
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Repita a senha" name="password_confirmation"/>
            </div>
        </div>

        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Atualizar</button>
</div>
</form>
</div><!-- /.box -->

</section>

</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_scripts'); ?>
<!-- InputMask -->
<script src="<?php echo e(env('CFURL').('/plugins/input-mask/jquery.inputmask.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js')); ?>"></script>
<script src="<?php echo e(env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js')); ?>"></script>
<script>
                $(function () {
                    $('.data').inputmask("99-99-9999");
                    $('.cpf').inputmask("999.999.999-99");
                    $("[data-mask]").inputmask();
                });
                function codigoSeg() {
                    if (confirm('Tem certeza?Sò continue essa operação se você tiver acesso ao e-mail vinculado a esse conta.')) {

                        $.ajax({
                            url: 'novaChave', success: function (result) {
                                alert(result);
                            }
                        });
                    }
                    return false;
                }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>