<?php $__env->startSection('htmlheader_title'); ?>
Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<!-- Main row -->
<div class="row">
    <section class="col-lg-12">
        <div class="callout callout-info">
            <h4>Parabéns, o seu cadastro foi realizado com sucesso!</h4>


    </section>


    <section class="col-lg-6">

        <div class="panel panel-info text-center" style="border: none;">
            <div class="panel-heading" style="background-color: #4b4b4b; color: #fff; border-color: none;">
                Efetuar Pagamento via PagSeguro
            </div>
            <br>
            <img src="http://www.atendimentopsiconline.com.br/wp-content/uploads/2015/06/logo-pagseguro.png" style="width: 70%; margin-top: -50px;">

            <a data-metodo="5" target="_blank" class="btn btn-lg btn-primary gerarBoleto">Clique aqui para pagar</a>
            <div class="clearfix"></div>
            <br>
            <br>
        </div>

    </section><!-- /.Left col -->

    <section class="col-lg-6">

        <div class="panel panel-info text-center" style="border: none;">
            <div class="panel-heading" style="background-color: #4b4b4b; color: #fff; border-color: none;">
                Via transferência bancária
            </div>
            <div class="panel-body">
                <a id="transferencia"  target="_blank" class="btn btn-lg btn-primary">Pagar Via transferência bancária</a>

            </div>
        </div>
</div>


</div><!-- /.row (main row) -->
<!-- REQUIRED JS SCRIPTS -->
<div id='transferenciaBan' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dados das contas</h4>

            </div>
            <div class="modal-body">
                <?= $config['deposito_contas'] ?>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('page_scripts'); ?>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>
    <script src="<?php echo e(env('CFURL').('/plugins/form/jquery.form.min.js')); ?>"></script>
    <script src="<?php echo e(env('CFURL').('/plugins/iCheck/icheck.min.js')); ?>"></script>
    <script type='text/javascript'>

function fBlockUi() {
    $.blockUI({
        message: "<h4>Por favor aguarde...</h4>",
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .5,
            color: '#fff'
        }
    });
}

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
});
$("#transferencia").click(function () {
    $("#transferenciaBan").modal();
});
$(".gerarBoleto").click(function () {
    elemento = $(this);
    elemento.html('Por favor aguarde...');
    $.ajax({
        'url': "?novopagamento=1&metodo=" + elemento.attr('data-metodo'),
        dataType: 'html',
        'success': function (txt) {
            if (txt == '') {
                alert('Houve uma falha ao executar a operação.Verifique se seus dados foram inseridos corretamente.');
            } else {
                elemento.removeClass('btn-primary');
                elemento.addClass('btn-success')
                elemento.html('Redirecionando...');
                setTimeout(function () {
                    location.href = txt;
                }, 2000);
            }

        }
    });

});
/**/
    </script>
    <div id='voucher' class="modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <p>


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Selecionar quantidade de Vouchers</h4>

                </div>
                <div class="modal-body">

                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="1">01</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="2">02</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="3">03</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="4">04</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="0">Nenhum</label>
                    </div>
                    <p>Valor total a pagar: R$<span id="valorPagar">200</span></p>


                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="gerarBoleto" class="btn btn-primary">Gerar Boleto</button>
                        </p>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <?php $__env->stopSection(); ?>

        <?php $__env->startSection('page_scripts'); ?>
        <!-- iCheck 1.0.1 -->
        <script src="<?php echo e(env('CFURL').('/plugins/blockUi/jquery.blockUI.js')); ?>"></script>
        <script src="<?php echo e(env('CFURL').('/plugins/form/jquery.form.min.js')); ?>"></script>
        <script src="<?php echo e(env('CFURL').('/plugins/iCheck/icheck.min.js')); ?>"></script>
        <script type='text/javascript'>

function fBlockUi() {
    $.blockUI({
        message: "<h4>Por favor aguarde...</h4>",
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .5,
            color: '#fff'
        }
    });
}

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
});

$("#gerarBoleto").click(function () {
    $('#gerarBoleto').html('Por favor aguarde...');
    $.ajax({
        'url': "?novopagamento=1",
        dataType: 'html',
        'success': function (txt) {
            if (txt == '') {
                alert('Houve uma falha ao gerar o boleto');
            } else {
                $('#gerarBoleto').removeClass('btn-primary');
                $('#gerarBoleto').addClass('btn-success')
                $('#gerarBoleto').html('Redirecionando...');
                setTimeout(function () {
                    location.href = txt;
                }, 2000);
            }

        }
    });

});
/**/
        </script>

        <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>