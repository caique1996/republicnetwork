<?php $__env->startSection('htmlheader_title'); ?>
Recuperar Senha
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?php echo e(url('/admin/home')); ?>"><b>Recuperar Senha</b></a>
        </div><!-- /.login-logo -->

        <?php if(session('status')): ?>
        <div class="alert alert-success">
            <?php echo e(session('status')); ?>

        </div>
        <?php endif; ?>

        <?php if(count($errors) > 0): ?>
        <div class="alert alert-danger">
            <strong>Whoops!</strong>
            <ul>
                <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="login-box-body">
            <p class="login-box-msg">Você recebera um email com um link, para redefinir a senha senha.</p>
            <form action="<?php echo e(url('/painel/password/email')); ?>" method="post">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" required placeholder="Email" name="email" value="<?php echo e(old('email')); ?>"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
   <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
                            <div class="g-recaptcha" data-sitekey="<?= \App\config::getConf()['recaptcha_key'] ?>"></div>
                            <br>
                        <?php } ?>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar Link</button>
                    </div><!-- /.col -->
                </div>
            </form>

            <br/>
            <a class="btn btn-default btn-block btn-flat" href="<?php echo e(url('/painel/login')); ?>">Login</a>

        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    <?php echo $__env->make('layouts.partials.scripts_auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
        <script src ='https://www.google.com/recaptcha/api.js' ></script>

    <?php } ?>
    <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
    </script>
</body>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>