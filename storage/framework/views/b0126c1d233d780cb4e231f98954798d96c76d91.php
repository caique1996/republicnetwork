<head>
    <meta charset="UTF-8">
    <title><?php echo $__env->yieldContent('htmlheader_title', 'Default'); ?> -    <?=App\config::getConf()['site_name']?>
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- FAVICON -->
    <link rel="icon" href="<?=App\config::getConf()['logo_2']?>" type="image/png">
    <link rel="shortcut icon" href="<?=App\config::getConf()['logo_2']?>" type="image/x-icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/bootstrap/css/bootstrap.min.css')); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo e(asset('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo e(asset('//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/dist/css/AdminLTE.min.css')); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/dist/css/skins/_all-skins.min.css')); ?>">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('plugins/iCheck/all.css')); ?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/morris/morris.css')); ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/jvectormap/jquery-jvectormap-1.2.2.css')); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/datepicker/datepicker3.css')); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/daterangepicker/daterangepicker-bs3.css')); ?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">
    <!-- Pace style -->
    <link rel="stylesheet" href="<?php echo e(env('CFURL').('/plugins/pace/pace.min.css')); ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php echo $__env->yieldContent('page_css'); ?>
</head>
