<form class="formAjax" method="post" action="<?php echo e(url('/admin/graduacao/salvar')); ?>">

    <?php echo e(csrf_field()); ?>

    <?php

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Graduação</h4>

    </div>
    <div class="modal-body">
        <input type="hidden" class="form-control"  placeholder="Nome" name="id" value="<?= $dados['id'] ?>"/>

        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="name" value="<?= $dados['name'] ?>"/>
        </div>
        <div class="form-group">
            <label>Texto de boas vindas</label>
            <textarea name="boas_vindas" >
                <?= $dados['boas_vindas'] ?> 
            </textarea>

        </div>
        <div class="form-group">
            <label>Prêmios</label>
            <textarea name="premios" >
                <?= $dados['premios'] ?> 

            </textarea>
        </div>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option selected="" value="<?= $dados['status'] ?>"><?= getStatus($dados['status']) ?></option>
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>PME(Porcentagem em números decimais)</label>
            <input type="text" step="any" class="form-control" required placeholder="PME" name="pme" value="<?= $dados['pme'] ?>" id='pme' />

        </div>
        <div class="form-group">
            <label>Pontuação necessária</label>
            <input type="number" class="form-control" required placeholder="Pontuação necessária" name="pontuacao" value="<?= $dados['pontuacao'] ?>"/>
        </div>
        <div class="form-group">
            <label>Porcentagem bônus de equipe</label>
            <input type="number" step="any" class="form-control" required="" name="porcentagem_equipe" value="<?= $dados['porcentagem_equipe'] ?>"/>
        </div>
        <?php
        $graduacoes = App\graduacoes::where('status', 1)->get();
        ?>

        <div class="form-group">
            <label>Ícone</label>
            <input type="url" class="form-control" required placeholder="Ícone" name="icone" value="<?= $dados['icone'] ?>"/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
</form>
<script>
    $('#valor').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#valorIndicacao').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
</script>


